#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# title           :azure_blob_genomics_cdg.py
# description     :Module to run Microsoft Genomics Analysis using Azure Cloud.
# author          :Gabriel S. Goncalves
# date            :01/10/2018
# version         :0.1
# python_version  :3.6.3
#
# Copyright Codenomics Biotecnologia Ltda (2018)
# 
# This file is part of Vision Platform
# 
# Can not be copied and/or distributed without the express
# permission of the author (gabriel@codenomics.cloud)
### Small changes made on april 15th 2019, by Gustavo R. Fernandes, to allow consecutive execution of all steps


import os
from azure.storage.blob import BlockBlobService
import re

"""

This module simplifies the use of Azure Blob and Microsoft Genomics by offering
a series of functions to manage files on the cloud and also locally. 

The functions are the following:

- upload_files_to_blob: Transfer local files to a blob folder on Azure
- microsoft_genomics_runner: Run the Best Practices Analisys pipeline o Azure 
cloud using the Microsft Genomics service
- download_files_from_azure_blob: transfer the files located on a specific blob
folder to yout local machine
- vcf_42_to_41: modify a few lines to change version of vcf from 4.2 to 4.1

"""

def upload_files_to_blob(blob_account_name, blob_account_key,
                         blob_folder, path_to_local_folder):
    '''
    This function upload local files to an azure blob folder.
    
    Args:
        blob_account_name (str): 
        blob_account_key (str):
        blob_folder (str):
        local_folder(str):
    
    Return:
        
    '''
    # Create object with blob account name and key
    block_blob_service = BlockBlobService(account_name=blob_account_name, 
                                          account_key=blob_account_key) 
    
    # Create destination folder on Azure Blob if non-existant
    if block_blob_service.create_container(blob_folder, fail_on_exist=False):
        print('Folder %s successfully created' % (blob_folder))
    else:
        print('Folder %s created already.' % (blob_folder))
    
    # Itearate over files on local_folder uploading it to blob_folder
    for subdir, dirs, files in os.walk(path_to_local_folder):
        for file in files:
            if file.endswith('fastq.gz'): ### GRF change
                #print(file)
                print(os.path.join(path_to_local_folder, file))
                path_to_file = os.path.join(path_to_local_folder, file)
                # Upload local files to Azure Blob
                block_blob_service.create_blob_from_path(blob_folder, file, path_to_file)
            
                # Monitor the copy process
                '''
                block_blob_properties = block_blob_service.get_blob_properties(blob_folder, file)
                copy_properties = block_blob_properties.properties.copy
                print('Copy properties status: ' + str(copy_properties.status))
                '''

    # List the files now on the azure blob folder
    generator = block_blob_service.list_blobs(blob_folder)
    print('\nFiles present on the azure blob folder %s:\n' % (blob_folder))
    for f in generator:
        print(f.name)

def microsoft_genomics_runner(ms_gen_url, ms_gen_key, 
                              blob_account_name, blob_account_key, 
                              blob_target_folder,process_name):
    """
    This function executes the Microsoft Genomics analysis pipeline on a blob
    folder on the users Azure Account.

    Args:
        ms_gen_url (str): 
        ms_gen_key (str): 
        blob_account_name (str): 
        blob_account_key (str): 
        blob_target_folder (str):

    Return:

    """
    # Create object with blob account name and key
    block_blob_service = BlockBlobService(account_name=blob_account_name, 
                                          account_key=blob_account_key)
       
    # Create output folder
    blob_folder_out = blob_target_folder.split('/')[-1] + '-output'
    block_blob_service.create_container(blob_folder_out, fail_on_exist=False)

    # Create an iterator of files present on the azure blob folder
    blob_generator = block_blob_service.list_blobs(blob_target_folder)
    
    #print('\nFiles present on the azure blob folder %s:\n' % (blob_folder))
    # Create list to receive processed files and list of files on 
    # blob_target_folder
    list_processed = []
    list_target_blob_folder = sorted([f.name for f in blob_generator])
    print(list_target_blob_folder)
    #print('List of processed files: {}'.format(list_processed) )

    # Iterate over the files submiting the pipeline to msgenomics
    """for r1_file in list_target_blob_folder:
        
        # Only selects fastq files that wasn't processed
        if r1_file.endswith('fastq.gz') and r1_file not in list_processed:
            
            # Define r2_file
            r2_file = r1_file.replace('_R1', '_R2') ## GRF alteration removed _ after 1 to allow na12878 to run
            print(r1_file)
            print(r2_file)
            
            # Continue only if r2_file on list_target_blob_folder
            if r2_file in list_target_blob_folder:
                # Append r1_file and r2_file to list_processed
                list_processed.extend([r1_file, r2_file])
    """
    sample_dict = {}
    for entry in sorted(os.listdir(basepath)):
        r1_list = []
        r2_list = []
        if '_R1_' in entry:
            if entry not in r1_list:
                sample = entry.split('_')
                sample_name = sample[0] + "_" + sample[1]
                for s in sorted(os.listdir(basepath)):
                    if sample_name in s:
                        if '_R1_' in s:
                            r1_list.append(s)
                        elif '_R2_' in s:
                            r2_list.append(s)
            sample_dict[sample_name] = [r1_list, r2_list]
    
    # Iterate over sample_dict
    for key, value in sample_dict.items():
        r1_file = ' '.join(value[0])
        r2_file = ' '.join(value[1])


        # Set the lines to run on shell
        l1_gnm_url = '/usr/local/bin/msgen submit --api-url-base {}'.format(
                                                    ms_gen_url) ### GRF alteration to correct msgen location
        l2_gnm_key = '--access-key {}'.format(ms_gen_key)
        l3_ref = '--process-args R=hg19m1'
        l4_str_acc = '--input-storage-account-name {}'.format(
                                                   blob_account_name)
        l5_str_key = '--input-storage-account-key {}'.format(
                                                    blob_account_key)
        l6_in_folder = '--input-storage-account-container {}'.format(
                                                   blob_target_folder)
        l7_r1 = '--input-blob-name-1 {}'.format(r1_file)
        l8_r2 = '--input-blob-name-2 {}'.format(r2_file)
        l9_str_acc = '--output-storage-account-name {}'.format(
                                                   blob_account_name)
        l10_str_key = '--output-storage-account-key {}'.format(
                                                   blob_account_key)
        l11_out_folder = '--output-storage-account-container {}'.format(
                                                   blob_folder_out)
        l12_process_name = '--process-name {}'.format(
                                                   process_name)

        # Unite the lines into one command
        cmd_united = '{} {} {} {} {} {} {} {} {} {} {} {}'.format(
            l1_gnm_url, l2_gnm_key, l3_ref, l4_str_acc, l5_str_key,
            l6_in_folder, l7_r1, l8_r2, l9_str_acc, l10_str_key, 
            l11_out_folder, l12_process_name)
        
        print(cmd_united)

        
        # Run the command using os.popen()
        #shell_runner = os.popen(cmd_united,"r")
        """
        while 1:
            line = shell_runner.readline()
            if not line: break
            print(line)
        """
        print('List of processed files: {}'.format(list_processed) )
        print(len(list_processed))
        print('\n\n')
        

def download_files_from_azure_blob(blob_account_name, blob_account_key, 
                              blob_target_folder, path_to_local_folder,
                              extension_formats=('.bai', '.bam', '.vcf')):
    """
    This function downloads different types of files from an azure blob to a 
    local folder.

    Args:
        ms_gen_url (str): 
        ms_gen_key (str): 
        blob_account_name (str): 
        blob_account_key (str): 
        blob_target_folder (str):
        path_to_local_folder (str):
        extension_formats (tuple): 

    Return:
        
    """
    extension_formats=('.bai', '.bam', '.vcf') ### GRF alteration: remove from function input
    # Create object with blob account name and key
    block_blob_service = BlockBlobService(account_name=blob_account_name, 
                                          account_key=blob_account_key)

    # Create generator for list of files on blob_target_folder
    generator = block_blob_service.list_blobs(blob_target_folder)

    # Iterate over generator
    for fl in generator:
        print(fl.name)

        # If fl has an extension define on extension_formats
        if fl.name.endswith(extension_formats):
            print('File extension ok: {}'.format(fl.name))

            # Tranfer file from blob to local folder
            block_blob_service.get_blob_to_path(blob_target_folder,
                              fl.name, path_to_local_folder + '/' + fl.name)


def fastqc_runner(folder):
    """
    Function to run fastqc tool on bam files inside folder.

    Args:
        folder (path): Path to folde with bam containing bam files.

    Return:
        Returns html file with the analisys and zip folder with processing information each original bam.
    """
    # Define list of files on folder
    bam_list = [f for f in os.listdir(folder) if f.endswith('.bam')]
    print('List of bam files on target folder: \n {}\n'.format(bam_list))
    
    # Iterate over bam_list
    for bam in bam_list:
        # Define sample number
        try:
            sample_number = re.match('^(\d+)_', bam).group(1)
        except:
            sample_number = bam[:6]

        # Define sample paths    
        path_to_bam = '{}/{}'.format(folder, bam)
        #path_to_bam_out = '{}/{}'.format(folder, sample_number + '_singlehit.bam')
        
        # Filter using samtools
        try:
            print('Running fastqc on sample {}'.format(bam))
            os.system('fastqc {}'.format(path_to_bam))
        except:
            print('Problems with fastqc for sample: {}'.format(bam))
       

def filter_bam_for_single_hit(folder):
    """
    Function to filter bam file for single hit reads.

    Args:
        folder (path): Path to folde with bam containing bam files.

    Return:
        Returns one new filtered bam and respective bai for each original bam.
    """
    # Define list of files on folder
    bam_list = [f for f in os.listdir(folder) if f.endswith('.bam')]
    
    # Iterate over bam_list
    for bam in bam_list:
        # Define sample number
        try:
            sample_number = re.match('^(\d+)_', bam).group(1)
        except:
            sample_number = bam[:6]

        # Define sample paths    
        path_to_bam = '{}/{}'.format(folder, bam)
        path_to_bam_out = '{}/{}'.format(folder, sample_number + '_singlehit.bam')
        
        # Filter using samtools
        try:
            print('Filtering single hits for sample {}'.format(bam))
            os.system('samtools view -h -q 1 {} | samtools view -Sb - > {}'.format(path_to_bam, 
                                                                              path_to_bam_out))
        except:
            print('Problems filtering for single hits for sample: {}'.format(bam))
        
        # Index new filtered bam file
        try:
            print('Indexing file {}'.format(path_to_bam_out))
            os.system('samtools index {} {}'.format(path_to_bam_out, path_to_bam_out + '.bai')) 

        except:
            print('Problems indexing file: {}'.format(path_to_bam_out)) 




def vcf_42_to_41(vcf_file):
    """
    This function changes the description of vcf from 4.2 to 4.1,
    so it can be used on Variant Interpreter.
    Arg:
        vcf_file (str): Path to the vcf file to be used.
    
    Return:
        vcf_41 (str): A new vcf file is created with the 4.1 version
    """
    
    # First, the vcf_file is loaded
    vcf_42 = open(vcf_file, 'r')

    # Check if vcf file is version 4.2
    #assert vcf_42.readlines()[0]=='##fileformat=VCFv4.2\n', "VCF não é da versão 4.2"

    # Create output file
    if 'fastq' in vcf_file:
        vcf_41_name = re.sub('.fastq.+', '_4_1.vcf', vcf_file)
        vcf_41 = open(vcf_41_name, 'w')
    else:
        vcf_41_name = re.sub('.vcf', '_4_1.vcf', vcf_file)
        vcf_41 = open(vcf_41_name, 'w')
    print(vcf_file)

    # Iterate over the lines making the changes
    for line in vcf_42.readlines():
        if line.startswith('##fileformat=VCFv4.2'):
            new_line = re.sub('4.2', '4.1', line)
            vcf_41.write(new_line)

        elif line.startswith('##reference'):
            new_line = '##reference=hg19m1.fa\n'
            vcf_41.write(new_line)

        else:
            vcf_41.write(line)

    vcf_41.close()
    vcf_42.close()

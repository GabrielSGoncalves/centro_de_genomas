# Tutorial de processamento de dados de NGS para o Centro de Genomas

O objetivo deste tutorial é instruir usuários do Centro de Genomas em como processar os dados provenientes de Sequenciadores de Nova Geração (NGS) seguindo os procedimentos padrão estabelecidos para o setor de Genética Humana.

***

## Usando o Basemount da Illumina
O Basemount (https://basemount.basespace.illumina.com/) é uma ferramenta desenvolvida pela Illumina para Linux (Debian) para se baixar arquivos presentes no BaseSpace (https://basespace.illumina.com/home).

### Instalando o Basemount na sua máquina
**1)** O primeiro passo a ser executado é instalar o programa em sua máquina local. Para isso, primeiro criar uma pasta em local desejado.

```shell
mkdir BaseSpace
cd BaseSpace
```

**2)** O próximo passo é usar o comando descrito abaixo para instalar o Basemount

```shell
sudo bash -c "$(curl -L https://basemount.basespace.illumina.com/install)"
```

**3)** Em seguida, voltar para a pasta anterior (para enxergar a pasta BaseSpace) e acessá-la usando o comando:

```shell
basemount BaseSpace
```

**4)** Você verá o logo do Basemount e o aviso abaixo na tela

```
You need to authenticate by opening this URL in a browser:
  https://basespace.illumina.com/oauth/device?code=XXXXX
```

**5)** Copiar o endereço no seu navegador e seguir os passos para autenticação.
Após este passo você poderá navegar dentro da pasta BaseSpace através do seu terminal usando comandos do Shell, mas estará na verdade enxergando as suas pastas presentes no seu BaseSpace.

```
tree
```

### Usando o Basemount
Após a instalação, toda vez que o usuário quiser acessar o seu BaseSpace através do Basemount ele deve executar o seguinte comando:

```shell
basemount BaseSpace
``` 

Após entrar na pasta 'BaseSpace', o usuário poderá navegar até a pasta de interesse e interagir com os arquivos de interesse. Para baixar um fastq para a sua máquina local usar:

```shell
cat /home/gabriel/BaseSpace/Projects/Trusight_cancer/Samples/361281/Files/361281_S3_L001_R1_001.fastq.gz > /home/gabriel/Desktop/361281_S3_L001_R1_001.fastq.gz 
```

Uma outra alternativa para transferir os arquivos entre o BaseSpace e uma máquina local é assim que montar o basemount, acessar os arquivos que estão no BaseSpace pelo gerenciador de arquivos do Linux (o Nautilus por exemplo) e copiar e colar para uma pasta local.  

***

## Usando o DNANexus
O DNANexus é um serviço de processamento e armazenamento de dados genômicos na nuvem que usa a infraestrutura de empresas como Amazon e Microsoft (AWS e Azure) para oferecer as ferramentas ao usuário final. Dentro da plataforma online do DNANexus foram construídos pipelines de análises para os painéis de genes gerados no Illumina Miseq. Para se executar uma análise dos dados provenientes de um sequenciador Illumina, seguir os passos a seguir:

**1)** Abrir o navegador no site www.dnanexus.com e fazer o login.

**2)** Para subir os dados para a nuvem do DNANexus, ir até a pasta 'Centro_de_Genomas/CdG_Rotina' e criar um novo diretório clicando no botão 'New Folder'. É recomendado nomear a nova pasta utilizando a convenção Nome do ensaio, número do ensaio e data no formato DDMMAA (Ex: Trusight_cancer_01_010119).

**3)** Dentro da pasta recém criada, clique no botão 'Add Data' para selecionar os arquivos a serem enviados para a plataforma. Em seguida pode-se arrastar os arquivos no seu computador até a janela que se abrirá no navegador e confirmar a operação clicando em 'Add Data'.

**4)** O próximo passo é selecionar o pipeline de análise 'Sentieon_Panel_Pipeline+SnpEff' na pasta 'Centro_de_Genomas/Pipelines'. Um esquema do pipeline irá ser mostrado na tela, com os arquivos de entrada e saída e os passos envolvidos no processo. Os únicos arquivos que o usuário deve selecionar são os fastq.gz referentes à leituras 1 (R1) e o fastq.gz referentes à leituras 2 (R2). É importante também selecionar a pasta para onde serão direcionados os arquivos de saída do pipeline, clicando em 'Workflow Actions > Set output folder'.

![Alt text](https://bytebucket.org/GabrielSGoncalves/vision/raw/1a286cf135b1e3ce07b6bc912893224deb1bf245/dnanexus_pipeline.png?token=a2cc80454cb1b65ffd85c0b004e114aa2ea6f189)

**5)** O DNANexus irá direcioná-lo para a página de monitoramento de análises (Monitor), e o usuário poderá acompanhar o processos. Caso ocorra algum problema, coluna Status irá mostrar o termo 'Failed'. Caso o processo ocorra com sucesso, a coluna de Status iŕa de 'In progress' para 'Done' e você poderá acessar os resultados.

## Usando o Command Line Interface (CLI) do DNANexus
O DNANexus oferece uma interface em linha de comando (CLI) para facilitar o processo de se trabalhar com vários arquivos. 

### Instalação do CLI dx

Para se gerar uma tabela TSV com as informações de pares de arquivos R1 e R2 de uma amostra usar o comando:

Para rodar o pipeline Sentieon_Panel_Pipeline+SnpEff

```shell
dx run Sentieon_Panel_Pipeline+SnpEff \
-istage-F8v31600F5ZXXP5VFz6vyff8.reads_fastqgzs=file-FFVZK300F5ZpQ9GZP7ZZbxf5 \
-istage-F8v31600F5ZXXP5VFz6vyff8.reads2_fastqgzs=file-FFVZK300F5Zbp3ZV1xb771Qg \
-istage-F8v31600F5ZXXP5VFz6vyff8.genomeindex_targz=file-B6qq53v2J35Qyg04XxG0000V \
-istage-F8v31600F5ZXXP5VFz6vyff8.genome_fastagz=file-B6qq93v2J35fB53gZ5G0007K
```

```shell
dx generate_batch_inputs -ireads_fastqgz='(.*)_S\d+_L001_R1_(.*).fastq.gz' \
-ireads2_fastqgz='(.*)_S\d+_L001_R2_(.*).fastq'
```

Para rodar o pipeline em vários amostras, usar o comando 'dx generate_batch_inputs' para gerar um arquivo TSV
com as informações das amostras na pasta:

```shell
dx generate_batch_inputs -ireads_fastqgz='(.*)_S\d+_L001_R1_(.*).fastq.gz' -ireads2_fastqgz='(.*)_S\d+_L001_R2_(.*).fastq'
```

E depois de remover a primeira linha do arquivo dx_batch.0000.tsv, rodar o script no shell:

```shell
cat dx_batch.0000.tsv | while read line; do set $line; dx run Sentieon_Panel_Pipeline+SnpEff \
-istage-F8v31600F5ZXXP5VFz6vyff8.reads_fastqgzs=file-FGP9py00F5ZZ4xgZ9x91K0z4 \
-istage-F8v31600F5ZXXP5VFz6vyff8.reads2_fastqgzs=file-FGP9py00F5ZVKFJF9VYjJz48 \
-istage-F8v31600F5ZXXP5VFz6vyff8.genomeindex_targz=file-B6qq53v2J35Qyg04XxG0000V \
-istage-F8v31600F5ZXXP5VFz6vyff8.genome_fastagz=file-B6qq93v2J35fB53gZ5G0007K; done
```

Dos diversos arquivos de saída do pipeline Sentieon_Panel_Pipeline+SnpEff, o único que será utilizado nas etapas seguinte será o com o sufixo '*snpEff.vcf.gz' e '*.bam[.bai]'. Para baixar os arquivos para sua máquina de uma forma prática, pode-se usar o comando 'dx download' do CLI do DNANexus:

Para os VCFs:
```shell
dx ls *snpEff.vcf.gz | cat | while read line; do dx download $line -o /home/gabriel/Desktop/$line; done
```

Para os BAMs:
```shell
dx ls *.bam | cat | while read line; do dx download $line -o /home/gabriel/Desktop/$line; done
```

Para os BAIs:
```shell
dx ls *.bai | cat | while read line; do dx download $line -o /home/gabriel/Desktop/$line; done
```

***

## Executando a análise de cobertura dos painéis
Para verificar as áreas de cobertura dos genes em questão dos painéis, o usuário pode utilizar o script 'coverage_analysis_v2.py'. Este script irá fornecer as métricas de profundidade necessárias para serem colocadas no laudo de cada paciente. Dentre as diversas informações, as principais geradas são as seguintes:


- Cobertura média ao longo das regiões analisadas
- Percentual de bases com cobertura acima de 20 leituras
- Percentual de bases com cobertura acima de 100 leituras
- Regiões com cobertura abaixo de 20 leituras


Para executar a ferramenta o usuário deve seguir os passos abaixo:


**1)** Baixar os arquivos BAM e BAI da resultantes da análise feita no DNANexus. Se as amostras todas tiverem sido sequenciadas para o mesmo grupo de genes (mesmo painel virtual), os arquivos BAM e BAI de mais uma amostra podem ser colocados na mesma pasta.

**2)** Abrir um terminal e executar o script 'coverage_analysis_v2.py' fornecendo como parâmetros a pasta contendo os arquivos BAM e BAI além do arquivo BED referente aos genes relacionados.

```shell
python3 coverage_analysis_v2.py <pasta alvo com arquivos> <arquivo BED>
``` 

**3)** O script irá criar uma pasta de saída com o mesmo nome da pasta alvo seguidos de '_output_' e data atual. Dentro desta pasta de saída o usuário irá encontrar uma pasta para cada amostra. Arquivos intermediários e os arquivos de entrada BAM e BAI da amostras respectiva também estarão armazenados nesta pasta. Dentro da pasta como o sufixo '_coverage_analisys' o usuário irá encontrar os arquivos com as informações relevantes.

**4)** Os dois arquivos de saída mais importantes são os com o sufixo '_coverage_overview.txt' e '_low_cov_regions_table.tsv'. No primeiron você encontrará as métricas globais para o painel como descrito abaixo:

``` 
Informações gerais de cobertura para a amostra 361918

Cobertura média do painel = 512.18
Percentual de bases acima de 20 leituras = 99.75%
Percentual de bases acima de 100 leituras = 97.88%
``` 

**5)** E no arquivo '_low_cov_regions_table.tsv' você encontrará as regiões abaixo de 20 leituras descritas em uma tabela como a mostrada abaixo

|Gene |Cromossomo|Região                     |Posição            |Número de bases|
|-----|----------|---------------------------|-------------------|-----------------|
|BAP1 |chr3      |BAP1.chr3.52437431.52437911|52437534 - 52437765|232|
|RET  |chr10     |RET.chr10.43606654.43606914|43606873 - 43606913|41|
|STK11|chr19     |STK11.chr19.1220371.1220505|1220372 - 1220372|1|
|STK11|chr19     |STK11.chr19.1220371.1220505|1220378 - 1220383|6|

***

## Usando o script para anotação de variantes

Esta etapa consiste em utilizar um pipeline para anotação das variantes a partir dos seguintes bancos de dados:

- dbSNP
- Clinvar
- ExAc
- 1000 genomes
- Abraom

Esta etapa pode ser utizada para se processar VCFs gerados a partir de sequenciadores Illumina ou IonTorrent. 
Para isto, o usuário deve em um terminal excutar o seguinte comando (estando na pasta do módulo 'parsing_vcf.py'):

```bash
python3 parsing_vcf.py <pasta contendo vcfs>
```

O script irá gerar dois arquivos de saída:

**1) annotations.tsv** : Este arquivo contém uma tabela com todas as variantes presentes no vcf original e as diversas colunas com informações dos bancos de dados descritos anteriormente.

**2) snpeff.tsv**: Este arquivo terá as anotações genômicas funcionais para as variantes do vcf. Cada variante pode ter uma ou mais anotações funcionais fornecidas pelo snpeff.  

***

## Importando os arquivos para a plataforma de curadoria de variantes VISION
A Plataforma de Medicina de Precisão (PMP) Vision irá fornecer ao usuário um ambiente de trabalho prático e eficiente para o que o mesmo consiga fazer a curadoria das variantes encontradas pelo ensaio em questão.
Para utilizar a plataforma de curadoria de variantes VISION, o usuário deve seguir os seguintes passos:

**1)** O usuário deverá entrar no site https://codenomics-pmp.herokuapp.com e clicar em 'Login Vision'. Colocar a o login e senha correspondentes para ter acesso à plataforma.

**2)** Na tela inicial da plataforma Vision (Dashboard), o usuário terá disponível uma lista de amostras já importadas. Para ser direcionado à página com as variantes de uma amostra em questão, clicar em cima de seu nome.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/dashboard.png)


**3)** Para o usuário importar novas amostras para a plataforma, ele deve clicar em 'Importar Arquivos' na parte superior esquerda da tela. A opção 'Importar Múltiples CSVs' irá aparecer na tela. Na caixa 'Anexar CSV' o usuário deverá escolher os arquivos processados 'annotations.tsv' e em 'Anexar CSV Snpeff' deve escolher os arquivos 'snepeff.tsv'.
Após apertar 'Confirmar', o usuário deve observar na tela de Dashboard a amostra referente aos arquivos importados.

**4)** Ao clicar em uma amostra no Dashboard, o usuário verá a página com as listas de variantes respectivas. Cada coluna contém informações relevantes para se fazer a classificação da variante, e mais informações podem ser obtidas ao clicar no botão verde de '+' na esquerda da tela.
As informações em azul também contém hiperlinks para bancos de dados externos, oferecendo ainda mais informações aos usuários.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_lista_variantes.png)

**5)** Filtros também podem ser utilizados para se obter variantes com características específicas. Para acessá-los, clicar no botão 'Filtros', escolher as diversas opções e clicar em 'Buscar'. Para limpar os filtros, o usuário pode clicar no botão 'Limpar filtro'.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_filtros.png)

**6)** Para fazer a classificação da variante, deve-se clicar no ícone da coluna 'Ações' e escolher a 'Interpretação' desejada.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_classificar_variante.png)


**7)** Após classificar as variantes de interesse, o usuário pode selecionar as que serão utilizadas no laudo clicando as caixas da coluna 'Variante' e clicando no botão 'Preview' na parte superior esquerda.

![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_marcar_preview.png)

**8)** E finalmente com a lista de variantes de interesse no Preview, pode exportá-las com tabela xlsx clicando em 'Exportar'.

![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_preview.png)






# Tutorial de processamento de dados de NGS para o Centro de Genomas

O objetivo deste tutorial é instruir usuários do Centro de Genomas em como 
processar os dados provenientes de Sequenciadores de Nova Geração (NGS) 
seguindo os procedimentos padrão estabelecidos para o setor de Genética Humana.

***

## Usando o Basemount da Illumina
O Basemount (https://basemount.basespace.illumina.com/) é uma ferramenta 
desenvolvida pela Illumina para Linux (Debian) para se baixar arquivos presentes
 no BaseSpace (https://basespace.illumina.com/home).

### Instalando o Basemount na sua máquina
**1)** O primeiro passo a ser executado é instalar o programa em sua máquina 
local. Para isso, primeiro criar uma pasta em local desejado.

```shell
mkdir BaseSpace
cd BaseSpace
```

**2)** O próximo passo é usar o comando descrito abaixo para instalar o 
Basemount

```shell
sudo bash -c "$(curl -L https://basemount.basespace.illumina.com/install)"
```

**3)** Em seguida, voltar para a pasta anterior (para enxergar a pasta 
BaseSpace) e acessá-la usando o comando:

```shell
basemount BaseSpace
```

**4)** Você verá o logo do Basemount e o aviso abaixo na tela

```
You need to authenticate by opening this URL in a browser:
  https://basespace.illumina.com/oauth/device?code=XXXXX
```

**5)** Copiar o endereço no seu navegador e seguir os passos para autenticação.
Após este passo você poderá navegar dentro da pasta BaseSpace através do 
seu terminal usando comandos do Shell, mas estará na verdade enxergando as suas 
pastas presentes no seu BaseSpace.

```
tree
```

### Usando o Basemount
Após a instalação, toda vez que o usuário quiser acessar o seu BaseSpace através
 do Basemount ele deve executar o seguinte comando:

```shell
basemount BaseSpace
``` 

Após entrar na pasta 'BaseSpace', o usuário poderá navegar até a pasta de 
interesse e interagir com os arquivos de interesse. Para baixar um fastq para a 
sua máquina local usar:

```shell
cat /home/gabriel/BaseSpace/Projects/Trusight_cancer/Samples/361281/Files/361281_S3_L001_R1_001.fastq.gz > /home/gabriel/Desktop/361281_S3_L001_R1_001.fastq.gz 
```

Uma outra alternativa para transferir os arquivos entre o BaseSpace e uma 
máquina local é assim que montar o basemount, acessar os arquivos que estão no 
BaseSpace pelo gerenciador de arquivos do Linux (o Nautilus por exemplo) e 
copiar e colar para uma pasta local.  

***
## Convenção de nomeação de corridas de sequenciamento do Illumina Miseq

Com intuito de facilitar a organização das corridas de sequenciamento feitas no 
Centro de Genomas foi estabelecida uma convenção para nomeação de pastas. O 
modelo segue um número em ordem sequencial para a respectiva corrida no 
sequenciador, nome do kit usado e número em ordem sequencial e data no formato 
DDMMAAA.

**Ex: run10-trusight-cancer6-29082018**

***

## Usando o Microsoft Genomics
O Microsoft Genomics é um serviço de análise secundária executado na nuvem Azure
da Microsoft. O pipeline é uma otimização do 
[BWA + GATK Best Practices pipeline](https://software.broadinstitute.org/gatk/best-practices/workflow?id=11145)
 estabelecido inicialmente pelo Broad Institute. Mais informações sobre o 
 Microsoft Genomics podem ser encontrados nos links abaixo:

 * [Documentation Microsoft Genomics](https://azure.microsoft.com/pt-br/services/genomics/)

 * [Microsoft Genomics White paper](https://azure.microsoft.com/en-us/resources/accelerate-precision-medicine-with-microsoft-genomics/)

Com intuito de se processar múltiplas amostras de forma programática foi criado 
um Jupyter notebook com todos os passos a serem seguidos pelo usuário. Para 
abrir o notebook, vá através do terminal do Linux até a pasta aonde se encontra 
o arquivo `Running_microsoft_genomics_cdg_tools.ipynb` e execute o comando:

```shell
jupyter notebook
```

O uma lista de arquivos irá se abrir no seu navegador padrão. Selecione o 
arquivo citado e siga os passos descritos abaixo.

1) Primeiro o usuário deve importar as bibliotecas utilizadas na análise:
```python
import os 
import re
import json
from azure_blob_genomics_cdg import *
```

2) Em seguida o usuário deve importar as informações de chaves secreta do 
serviço Microsoft Genomics e Azure Blob a partir de um arquivo `env.json` em sua
pasta.
```python
with open('env.json') as data_file:    
    dict_variables = json.load(data_file)

Genomics_account_URL = dict_variables['Genomics_account_URL']
Genomics_account_access_key = dict_variables['Genomics_account_access_key']
Storage_account_name = dict_variables['Storage_account_name']
Storage_account_access_key = dict_variables['Storage_account_access_key']
```
3) O próximo passo é definir as variáveis com endereço local da pasta contendo
o arquivos fastq a serem analisados, e o nome da pasta de destino no Azure Blob.

4) Para que o usuário possa executar o pipeline de análise, ele precisa subir 
os arquivos para um Blob (pasta no Azure). Para isso, ele pode usar a função 
`upload_files_to_blob` com as variáveis já definidas anteriormente.
```python
upload_files_to_blob(Storage_account_name, Storage_account_access_key,
                         blob_folder_name, path_to_local_folder)
```

5) Para checar o estado do serviço Microsoft Genomics, podemos o comando de 
shell abaixo como magic command no Jupyter notebook:
```python
!msgen list -u $Genomics_account_URL -k $Genomics_account_access_key
```

6) E finalmente disparar os jobs no serviço da Microsoft Genomics usando a 
função `microsoft_genomics_runner`: 

```python
microsoft_genomics_runner(Genomics_account_URL, Genomics_account_access_key, 
                              Storage_account_name, Storage_account_access_key, 
                              blob_folder_name)
```

7) O usuário pode checar se os jobs foram corretamente iniciados rodando o mesmo
 comando do passo *5*. Pode-se monitorar se todos os jobs iniciados foram 
 terminados (verificar pelo `status: Completed successfully`) para se dar 
 prosseguimento à análise.

8) O próximo passo consiste em baixar os arquivos processados para a sua máquina
 local através com auxílio da função `download_files_from_azure_blob`: 

```python
download_files_from_azure_blob(
                              Storage_account_name, Storage_account_access_key, 
                              blob_folder_name_out, path_to_local_folder,
                              extension_formats=('.bai', '.bam', '.vcf'))
```

9) Para se filtrar as leituras com apenas um local de mapeamento (single-hits),
 o usuário pode usar a função `filter_bam_for_single_hit` usando como parâmetro
o caminho até a pasta com os arquivos baixados:

```python
filter_bam_for_single_hit('path/to/target/folder')
```

10) Para avaliar a qualidade dos dados brutos gerados, o usuário pode usar a 
ferramenta `fastqc`. Para isso, a função `fastqc_runner` pode ser usada na pasta
 alvo, gerando os arquivos de saída para cada arquivo bam presente. 
 
```python
fastqc_runner('path/to/target/folder')
```

***

## Executando a análise de cobertura dos painéis
Para verificar as áreas de cobertura dos genes em questão dos painéis, o usuário
 pode utilizar o script 'coverage_analysis_v3.py'. Este script irá fornecer as 
métricas de profundidade necessárias para serem colocadas no laudo de cada 
paciente. 

Para executar a ferramenta o usuário deve seguir os passos abaixo:


1) Baixar os arquivos BAM e BAI das amotras resultantes da análise feita no 
Microsoft Genomics. Se as amostras todas tiverem sido sequenciadas para o 
mesmo grupo de genes (mesmo painel virtual), os arquivos BAM e BAI de mais uma 
amostra podem ser colocados na mesma pasta.

2) Abrir um terminal e executar o script 'coverage_analysis_v3.py' fornecendo 
como parâmetros a pasta contendo os arquivos BAM e BAI, além do arquivo BED 
referente aos genes relacionados e o parâmetro 'illumina'.

```shell
python3 coverage_analysis_v3.py <pasta alvo com arquivos> <arquivo BED> illumina
``` 

3) O script irá criar uma pasta de saída com o mesmo nome da pasta alvo seguidos
 de '_output_' e data atual. Dentro desta pasta de saída o usuário irá encontrar
 uma pasta para cada amostra. Arquivos intermediários e os arquivos de entrada 
BAM e BAI da amostras respectiva também estarão armazenados nesta pasta. Dentro 
da pasta como o sufixo '_coverage_analisys' o usuário irá encontrar os arquivos 
com as informações relevantes.

4) Os dois arquivos de saída mais importantes são os com os sufixos 
'_coverage_overview.txt' e '_low_cov_regions_table.tsv'. No primeiron você 
encontrará as métricas globais para o painel como descrito abaixo:

``` 
Informações gerais de cobertura para a amostra 411342

Cobertura média do painel = 95.0
Percentual de bases acima de 10 leituras = 98.4%
Percentual de bases acima de 20 leituras = 95.73%
Percentual de bases acima de 100 leituras = 33.65%
``` 

5) E no arquivo '_low_cov_regions_table.tsv' você encontrará as regiões abaixo 
de 20 leituras descritas em uma tabela como a mostrada abaixo:

|Gene |Cromossomo|Região                     |Posição            |Número de bases|
|-----|----------|---------------------------|-------------------|-----------------|
|BAP1 |chr3      |BAP1.chr3.52437431.52437911|52437534 - 52437765|232|
|RET  |chr10     |RET.chr10.43606654.43606914|43606873 - 43606913|41|
|STK11|chr19     |STK11.chr19.1220371.1220505|1220372 - 1220372|1|
|STK11|chr19     |STK11.chr19.1220371.1220505|1220378 - 1220383|6|

***

## Usando o script para anotação de variantes para a utilização na Plataforma
Vision

Esta etapa pode ser utizada para se processar VCFs gerados a partir de 
sequenciadores Illumina ou IonTorrent. Para isto, o usuário deve em um terminal 
excutar o seguinte comando (estando na pasta do módulo 'parsing_vcf.py'):

```shell
python3 parsing_vcf.py <pasta contendo vcfs>
```
O script irá gerar dois arquivos de saída:

1) annotations.tsv : Este arquivo contém uma tabela com todas as variantes 
presentes no vcf original e as diversas colunas com informações dos bancos de 
dados descritos anteriormente.

2) snpeff.tsv: Este arquivo terá as anotações genômicas funcionais para as 
variantes do vcf. Cada variante pode ter uma ou mais anotações funcionais 
fornecidas pelo snpeff.  

***

## Importando os arquivos para a plataforma de curadoria de variantes VISION
A Plataforma de Medicina de Precisão (PMP) Vision irá fornecer ao usuário um 
ambiente de trabalho prático e eficiente para o que o mesmo consiga fazer a 
curadoria das variantes encontradas pelo ensaio em questão.
Para utilizar a plataforma de curadoria de variantes VISION, o usuário deve 
seguir os seguintes passos:

1) O usuário deverá entrar no site https://codenomics-pmp.herokuapp.com e clicar
 em 'Login Vision'. Colocar a o login e senha correspondentes para ter acesso à 
 plataforma.

2) Na tela inicial da plataforma Vision (Dashboard), o usuário terá disponível 
uma lista de amostras já importadas. Para ser direcionado à página com as 
variantes de uma amostra em questão, clicar em cima de seu nome.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/dashboard.png)


3) Para o usuário importar novas amostras para a plataforma, ele deve clicar em 
'Importar Arquivos' na parte superior esquerda da tela. A opção 'Importar 
Múltiples CSVs' irá aparecer na tela. Na caixa 'Anexar CSV' o usuário deverá 
escolher os arquivos processados 'annotations.tsv' e em 'Anexar CSV Snpeff' 
deve escolher os arquivos 'snepeff.tsv'. Após apertar 'Confirmar', o usuário 
deve observar na tela de Dashboard a amostra referente aos arquivos importados.

4) Ao clicar em uma amostra no Dashboard, o usuário verá a página com as listas 
de variantes respectivas. Cada coluna contém informações relevantes para se 
fazer a classificação da variante, e mais informações podem ser obtidas ao 
clicar no botão verde de '+' na esquerda da tela.
As informações em azul também contém hiperlinks para bancos de dados externos, 
oferecendo ainda mais informações aos usuários.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_lista_variantes.png)

5) Filtros também podem ser utilizados para se obter variantes com 
características específicas. Para acessá-los, clicar no botão 'Filtros', 
escolher as diversas opções e clicar em 'Buscar'. Para limpar os filtros, o 
usuário pode clicar no botão 'Limpar filtro'.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_filtros.png)

6) Para fazer a classificação da variante, deve-se clicar no ícone da coluna 
'Ações' e escolher a 'Interpretação' desejada.


![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_classificar_variante.png)


7) Após classificar as variantes de interesse, o usuário pode selecionar as que 
serão utilizadas no laudo clicando as caixas da coluna 'Variante' e clicando no 
botão 'Preview' na parte superior esquerda.

![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_marcar_preview.png)

8) E finalmente com a lista de variantes de interesse no Preview, pode 
exportá-las com tabela xlsx clicando em 'Exportar'.

![Alt text](https://bitbucket.org/GabrielSGoncalves/vision/raw/master/vision_preview.png)

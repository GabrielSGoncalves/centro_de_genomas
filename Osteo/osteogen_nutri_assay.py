import os
import pandas as pd
import re
import bs4
import pdfkit
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger

def processing_osteogen(csv_file):
    # Let pandas receive long string
    pd.set_option('display.max_colwidth', -1)

    # Dictionaris and tables of genes and rs
    gene_rs_table ={'rs3736228':'LPR5','rs4988321':'LPR5','rs4355801':
    'TNFRSF11B (OPG)', 'rs2062377':'TNFRSF11B (OPG)','rs4792909':'SOST',
    'rs4869742':'ESR1', 'rs6426749':'ZBTB40','rs1373004':'DKK1','rs884205':
    'TNFRSF11A (RANK)', 'rs9533090':'TNFSF11 (RANKL)','rs11692564':'EN1',
    'rs587777005':'LGR4', 'rs13182402':'ALDH7A1','rs784288':'MECOM','rs7521902':
    'WNT4', 'rs1800012':'COL1A1','rs1544410':'VDR (BsmI)','rs7975232':
    'VDR (ApaI)', 'rs731236':'VDR (TaqI)','rs2282679':'GC','rs1800795':'IL6'}

    table_rs_nutri = ['rs2282679', 'rs1800795', 'rs731236',
                      'rs7975232', 'rs1544410', 'rs1800012']

    table_rs_osteogen = ['rs7521902', 'rs6426749', 'rs11692564', 'rs784288',
    'rs13182402', 'rs4869742', 'rs4355801', 'rs2062377', 'rs1373004',
    'rs587777005', 'rs4988321', 'rs3736228', 'rs9533090', 'rs4792909',
    'rs884205']

    table_inverted_results_osteo = ['rs11692564', 'rs784288', 'rs4355801',
                                    'rs1373004', 'rs884205', ]


    # Define the path to the pictures
    red_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha vermelha.jpg'alt='Image not found'>"
    yellow_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha amarela.jpg'alt='Image not found'>"
    green_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha verde.jpg'alt='Image not found'>"

    # Load the pdf template files
    pdfOne = PdfFileReader(open("modelo_laudos/Laudo_Osteogen_I.pdf", "rb"))
    pdfTwo = PdfFileReader(open("modelo_laudos/Laudo_Osteogen_II.pdf", "rb"))
    pdfThree = PdfFileReader(open("modelo_laudos/Laudo_Osteogen_III.pdf", "rb"))


    # Try to open csv
    file_error = False
    try:
        df = pd.read_csv(csv_file, sep=';')
    except IOError:
        print("Arquivo %s não encontrado." % csv_file)
        file_error = True
    # If csv_file was correctly loaded
    if file_error == False:
        df = pd.read_csv(csv_file, sep=';')

    # Filter the dataframe for only the SNPs labeled 'Hotspot' in collumn 'Allele source'
    df = df.loc[df['Allele Source'] == "Hotspot"]

    # Get a list of patients by selecting the 'Sample Name' collumn
    sample_names =set(df["Sample Name"])

    # Set HTML head to use UTF-8
    html_head =  '''<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="tables_style.css">
      </head>
      <img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/osteogen_cabecalho.png' align="middle" alt='Image not found'></td>
     '''

    # Iterate over sample_names
    for sample in sample_names:
        print(sample)
        try:
            # Get dataframe with only SNPs from current sample
            df_sample = df.loc[df['Sample Name'] == sample]

            # First process only the SNPs related to the nutri and osteogen assays
            df_sample_nutri = df_sample[df_sample["Allele Name"].isin(table_rs_nutri)]
            df_sample_osteogen = df_sample[df_sample["Allele Name"].isin(table_rs_osteogen)]

            # Create "results" column
            df_sample_nutri.insert(0, 'results', '')
            df_sample_nutri.insert(0, 'genotype', '')
            df_sample_nutri.insert(0, 'gene', '')
            df_sample_osteogen.insert(0, 'results', '')
            df_sample_osteogen.insert(0, 'genotype', '')
            df_sample_osteogen.insert(0, 'gene', '')
            df_sample_osteogen.insert(0, 'score', '')

            # Create merger for the pdf
            merger = PdfFileMerger()

            # Iterate over df_sample_nutri dataframe
            for index, row in df_sample_nutri.iterrows():
                print(row['Allele Name'])

                # Set gene name to the row
                df_sample_nutri.set_value(index, "gene",
                gene_rs_table[row['Allele Name']])

                if row['Allele Name'] in table_rs_nutri:
                    if row['Allele Call'] == 'Absent':
                        df_sample_nutri.set_value(index, "genotype",
                        str(row['Ref']*2))
                        df_sample_nutri.set_value(index, "results",
                        green_ball)

                    elif row['Allele Call'] == 'Heterozygous':
                        df_sample_nutri.set_value(index, "genotype",
                                                  str(row['Ref'] +
                                                  row['Variant']))
                        df_sample_nutri.set_value(index, "results",
                        yellow_ball)

                    elif row['Allele Call'] == 'Homozygous':
                        df_sample_nutri.set_value(index, "genotype", str(row['Variant']*2))
                        df_sample_nutri.set_value(index, "results", red_ball)

            # Select columns to output from nutri assay and rename them
            df_sample_nutri = df_sample_nutri[['gene','Allele Name','Ref','Variant','genotype',
                                               'results']]
            df_sample_nutri = df_sample_nutri.rename(columns={'gene': 'GENE', 'Allele Name': 'dbSNP',
                                            'Ref': 'ALELO NORMAL','Variant': 'ALELO DE RISCO',
                                            'genotype': 'GENÓTIPO', 'results': 'RISCO'})
            # Write datframes to csv files
            df_sample_nutri = df_sample_nutri.reset_index()
            df_sample_nutri = df_sample_nutri.drop('index', 1)
            df_sample_nutri.to_csv(sample + "_nutrigenetica_tabela_resultados.csv")

            # Write nutri dataframe to HTML
            with open(sample + "_nutri.html", 'w') as nutri_html:
                nutri_html.write(html_head + df_sample_nutri.to_html(escape=False, index = False))

            # Convert HTML to pdf
            pdfkit.from_file(sample + "_nutri.html", sample + '_nutri.pdf')


            # Iterate over df_sample_osteogen dataframe
            for index, row in df_sample_osteogen.iterrows():
                # Set gene name to the row
                df_sample_osteogen.set_value(index, "gene", gene_rs_table[row['Allele Name']])

                # Iterate over the rows setting the values to the created columns
                if row['Allele Name'] in table_rs_osteogen:
                    if row['Allele Call'] == 'Heterozygous':
                        df_sample_osteogen.set_value(index, "genotype",
                                                  str(row['Ref'] + row['Variant']))
                        df_sample_osteogen.set_value(index, "results", yellow_ball)
                        df_sample_osteogen.set_value(index, "score", 1)

                    else:
                        if str(row['Allele Name']) not in table_inverted_results_osteo:
                            if row['Allele Call'] == 'Absent':
                                df_sample_osteogen.set_value(index, "genotype", str(row['Ref']*2))
                                df_sample_osteogen.set_value(index, "results", green_ball)
                                df_sample_osteogen.set_value(index, "score", 0)

                            elif row['Allele Call'] == 'Homozygous':
                                df_sample_osteogen.set_value(index, "genotype", str(row['Variant']*2))
                                df_sample_osteogen.set_value(index, "results", red_ball)
                                df_sample_osteogen.set_value(index, "score", 2)

                        else:
                            if row['Allele Call'] == 'Homozygous':
                                df_sample_osteogen.set_value(index, "genotype", str(row['Variant']*2))
                                df_sample_osteogen.set_value(index, "results", green_ball)
                                df_sample_osteogen.set_value(index, "score", 2)

                            elif row['Allele Call'] == 'Absent':
                                df_sample_osteogen.set_value(index, "genotype", str(row['Ref']*2))
                                df_sample_osteogen.set_value(index, "results", red_ball)
                                df_sample_osteogen.set_value(index, "score", 0)

            # Select columns to output from nutri assay and rename them
            df_sample_osteogen = df_sample_osteogen[['gene','Allele Name','Ref','Variant','genotype',
                                               'results','score']]
            df_sample_osteogen = df_sample_osteogen.rename(columns={'gene': 'GENE', 'Allele Name': 'dbSNP',
                                            'Ref': 'ALELO NORMAL','Variant': 'ALELO DE RISCO',
                                            'genotype': 'GENÓTIPO', 'results': 'RISCO',
                                            'score': 'BONALDI SCORE'})
            # Write datframes to csv files
            df_sample_osteogen = df_sample_osteogen.reset_index()
            df_sample_osteogen = df_sample_osteogen.drop('index', 1)
            df_sample_osteogen.to_csv(sample + "_osteogen_tabela_resultados.csv")

            # Write osteogen dataframe to HTML
            with open(sample + "_osteo.html", 'w') as nutri_html:
                nutri_html.write(html_head + df_sample_osteogen.to_html(escape=False, index = False))

            # Convert HTML to pdf
            pdfkit.from_file(sample + "_osteo.html", sample + '_osteo.pdf')

            # Load PDF tables
            pdfTable_nutri = PdfFileReader(open(sample + '_nutri.pdf', "rb"))
            pdfTable_osteo = PdfFileReader(open(sample + '_osteo.pdf', "rb"))

            # Add PDF files to pdf object
            merger.append(pdfOne)
            merger.append(pdfTable_nutri)
            merger.append(pdfTwo)
            merger.append(pdfTable_osteo)
            merger.append(pdfThree)

            # Write pdf object to
            '''
            outputStream = open(sample + "_laudo.pdf", "wb")
            output.write(outputStream)
            outputStream.close()
            '''
            merger.write(sample + "_laudo.pdf")

        except:
            print("Amostra " + sample + " não encontrada na tabela.")
    df = df.reset_index()
    #return(df)

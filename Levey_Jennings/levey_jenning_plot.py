import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages


def plot_levey_jennings(list_values, pdf_name, table_name, log = False):

    # Check if values need to be converted to log
    if log == True:
        list_values = [np.log10(x) for x in list_values]

    # Get metrics from values
    values_mean = np.mean(list_values)
    values_std = np.std(list_values)

    #
    fig = plt.figure(figsize=(16, 7), dpi=600)

    # Set the labels for y-ticks
    y_ticks = ["-3DP", "-2DP", "-1DP", "média", "+1DP", "+2DP", "+3DP", ]
    x_ticks = range(1, len(list_values) + 1, 2)

    # Set scale of axis
    y_axis_low = (values_mean - (3.5 * values_std))
    y_axis_high = (values_mean + (3.5 * values_std))
    plt.axis([-0.5, len(list_values)- 0.5, y_axis_low , y_axis_high])
    #plt.yticks([values_mean - 3 * values_std, values_mean - 2 * values_std, values_mean - values_std ,
    #            values_mean, values_mean + values_std, values_mean + 2 * values_std, values_mean + 3 * values_std])
    plt.yticks([values_mean - 3 * values_std, values_mean - 2 * values_std, values_mean - values_std ,
                values_mean, values_mean + values_std, values_mean + 2 * values_std, values_mean + 3 * values_std],
               y_ticks)
    plt.grid('on', axis='y' )
    plt.xticks(range(0,len(list_values) + 1, 2), x_ticks)
    # Set plot
    plt.plot(list_values,'go-', color='black')
    #plt.rcParams["figure.figsize"] = [6,6]
    #plt.figure(figsize=(200,100))
    #plt.set_size_inches(18.5, 10.5)

    # Change the background color
    plt.axhspan(values_mean - values_std, values_mean + values_std, color='green', alpha=0.25)
    plt.axhspan(values_mean + values_std, values_mean + 2 * values_std, color='yellow', alpha=0.40)
    plt.axhspan(values_mean - values_std, values_mean - 2 * values_std, color='yellow', alpha=0.40)
    plt.axhspan(values_mean + 2 * values_std, values_mean + 3 * values_std, color='tomato', alpha=0.40)
    plt.axhspan(values_mean - 2 * values_std, values_mean - 3 * values_std, color='tomato', alpha=0.40)
    plt.axhspan(values_mean + 3 * values_std, values_mean + 3.5 * values_std, color='tomato', alpha=0.60)
    plt.axhspan(values_mean - 3 * values_std, values_mean - 3.5 * values_std, color='tomato', alpha=0.60)


    # Save plot to pdf
    pp = PdfPages(pdf_name)
    pp.savefig()
    pp.close()

    # Set title and labels
    plt.title(table_name)
    plt.show()

## Tutorial para análise do ensaio de genotipagem do HCV no IonTorrent

Este tutorial tem como objetivo instruir o usuário na geração dos arquivos de entrada para a configuração do sequenciamento em uma máquina IonTorrent e posterior análise dos resultados.

Todo o processo de criação das lista de amostras à geração de laudos é automatizada por scripts em Python e o usuário poderá executar todo o processo seguindo os passos descritos abaixo.

1) Após cada rotina de extração no robô M2000 da Abbott, um relatório será gerado contendo entre outras informações o número de recipiente da amostra e sua posição na placa de 96 poços. Estes arquivos podem ser encontrados na pasta do servidor:

smb://192.168.0.44/m2000sp

O usuário deverá buscar os arquivos correspondentes e ...

2) O próximo deverá ser abrir o Jupyter Notebook (a partir do Anaconda) e ir até o notebook 'Rotina_HCV_PGM_v1'. Neste notebook o usuário poderá ler as instruções detalhadas necessárias para processar os arquivos .tor do M2000.

3) Após importar o módulo 'm2000_XML_parser' o usuário poderá utilizar a função 'm2000_xml_parser' para processar o arquivo .tor(um XML no caso). Além do nome do arquivo .tor o usuário deverá indicar qual a placa de primers com barcodes que será utilizada para a PCR. Os valores indicados deverão 'p1', 'p2', 'p3' ou 'p4'. A função irá retornar uma tabela com o número de recipiente e posição respectiva na placa, sendo nomeada pela conveção 'hcvngs_p[1234]_data[ddmmaaaa].tsv'. 
Cada uma das 4 placas deverá ser processada por esta função, gerando-se 4 tabelas com 96 amostras, com o número do barcode e nome da amostra para cada corrida no IonTorrent.

4) Após ter se gerado as 4 tabelas a partir dos 4 arquivos .tor saídos do M2000, deve-se utilizar a função  'ion_sample_table_generator' descrita no notebook. Esta função tem como parâmetros o nome dos 4 arquivos gerados pelo passo anterior. Importante lembrar que a ordem dos arquivos deve ser do p1 ao p4, para que a função não indique nenhum erro. Caso a função seja executada com sucesso, um arquivo de saída será gerado.
Este arquivo é também uma tabela que segue o modelo de entrada de dados no IonTorrent para definição de amostras e barcodes. 

5) O próximo passo é ir até o Torrent Server (192.168.0.69 ou 192.168.0.23) e fazer o login na conta (usuário: ionadmin, senha: ionadmin)

![Alt text](https://bytebucket.org/GabrielSGoncalves/centro_de_genomas/raw/d1e2612624dbe7b1f54eab7a1a7715aa1bf928f3/HCV_IonTorrent/iontorrent_login.png)

6) O próximo passo é ir até Plan > Templates e selecionar o modelo de corrida 'HCV_IonTorrent_PGM_Template'. Este modelo já está configurado para os kits e parâmetros utilizados em nossa rotina de genotipagem do HCV.

![Alt text](https://bytebucket.org/GabrielSGoncalves/centro_de_genomas/raw/d1e2612624dbe7b1f54eab7a1a7715aa1bf928f3/HCV_IonTorrent/template_run.png)

7) O usuário deverá renomear a corrida utilizando a convenção 'HCVNGS_[número da corrida]_[data, ddmmaaaa]' como por exemplo 'HCVNGS_003_110518'. 

![Alt text](https://bytebucket.org/GabrielSGoncalves/centro_de_genomas/raw/d1e2612624dbe7b1f54eab7a1a7715aa1bf928f3/HCV_IonTorrent/run_name.png)

8) Em seguida o usuário deverá selecionar a tabela de amostras gerada no passo 4 clicando em 'Load Samples Table'.

![Alt text](https://bytebucket.org/GabrielSGoncalves/centro_de_genomas/raw/d1e2612624dbe7b1f54eab7a1a7715aa1bf928f3/HCV_IonTorrent/loadsamples.png)

9) As 384 amostras deverão estar listadas na tabela da página. Importante checar se as amostras e os barcodes estão corretamente correlacionadas.

![Alt text](https://bytebucket.org/GabrielSGoncalves/centro_de_genomas/raw/d1e2612624dbe7b1f54eab7a1a7715aa1bf928f3/HCV_IonTorrent/samples_list.png)

10) Em seguida o usuário deve clicar no botão 'Upload plan' para salvar o plano da corrida no Torrent Server. Se tudo tiver sido corretamente preenchido o plano poderá ser selecionado diremente do sequenciador IonTorrent para que a corrida consiga ser iniciada.


### Análise dos resultados

Para executar a análise dos dados gerados pelo sequenciador Ion Torrent.

1) Abrir o Filezilla e conectar ao servidor do IonTorrent (192.168.0.69).

2) Na metade da direita do Filezilla ir até a pasta:

/results/analysis/output/Home/<Nome_da_corrida>/plugin_out/FileExporter_out

3) Selecionar todos os arquivos bam/bai e arrastar para uma pasta local (metade esquerda do Filezilla)

4) Abrir o terminal (Ctrl + Alt + T) e ir até a pasta 'hcv_pgm'

> cd /home/gabriel/Documents/Repos/hcv_pgm

5) Baixar as tabelas de entrada no Ion PGM (sample sheat) que determina qual amostra está atrelada a cada barcode.

6) Baixar tabela do Pleres com informações dos pacientes e converter para arquivo csv separado por ';'.

7) Executar o pipeline usando o comando:
```shell
> python3 HCV_Ion_pipeline_v2.py <main_folder> <pleres_table>
```
Ex:
```shell
> python3 HCV_Ion_pipeline_v1.1.py /home/gabriel/Documents/hcv_chip10_teste \
   /home/gabriel/Documents/Entrada_Geno_HCV_2018_junho_julho.csv 
```
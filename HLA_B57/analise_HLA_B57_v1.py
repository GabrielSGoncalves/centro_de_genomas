#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Gabriel dos Santos Goncalves
# Sao Paulo, Brazil - May 2018

import os
import pandas as pd
import re
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from datetime import datetime

def processando_dados_HLA_B57(multifasta, lista_amostras):
    # Set date
    date = datetime.now().strftime("%y%m%d")

    # Set Multifasta object
    records = list(SeqIO.parse(multifasta, "fasta"))
    
    # Create dataframe to receive information from samples
    df_hla = pd.DataFrame(columns=['NumAtendimento', 'NomePaciente',
        'Resultado', 'AleloF', 'AleloR', 'Sisgeno', 'DataEntrada', 
        'CodigoExame', 'SequenciaF', 'SequenciaR'])

    # Create file to receive output and iterate over records
    try:
        pleres_df = pd.read_excel(lista_amostras, sheet_name=0)
    except:
        print('Problema com tabela de amostras %s' % (lista_amostras))
        return()

    # Get general information about data and print header message
    list_names = []
    for name in [x.name for x in records]:
        sample_number_search = re.search('(.+)_HCP', name)
        try:
            sample_number = str(sample_number_search.group(1))
            list_names.append(sample_number)
        except:
            continue
    
    list_unique_names = set(list_names)
    
    print('Arquivo processado %s.' % (multifasta))
    print('Número total de sequências: %s' % (len(list_names)))
    print('Número total de amostras: %s' % (len(list_unique_names)))
    print('\nLog de problemas\n')
    
    dict_seqs={}
    for record in records:
        # Get sample number 
        sample_number_search = re.search('(.+)_HCP', record.name)
        try:
            sample_number = str(sample_number_search.group(1))
        except:
            print('Sequência %s com padrão de número de recipiente inconsistente.' % (record.name))
            continue
        # Get primer orientation
        orientation_search = re.search('.+_HCP-MS_(.)_', record.name)
        try:
            orientation = orientation_search.group(1)
        except:
            print('Orientação do primer da %s com padrão de inconsistente.' % (record.name))
            continue
        # Get SISGENO number
        try:
            cell = pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['Observação (SISGENO)']
            sisgeno_number = re.search('(29.+_4)',cell).group(1)
        except:
            
            print('Problema em busca de SISGENO com %s.' % (record.name))
            continue

        # Get allele
        allele = ''
        if orientation.lower() == 'f':
            try:
                allele = re.search('TTCCCCTG(\w)', str(record.seq)).group(1)
            except:
                print('Problema em encontrar alelo alvo na sequência alvo da amostra %s' % (record.name))

        elif orientation.lower() == 'r':

            try:
                allele = re.search('AGCTGCC(\w)', str(record.seq)).group(1)
            except:
                print('Problema em encontrar alelo alvo na sequência alvo da amostra %s' % (record.name))
      
        
        # Check if df_hla already with information about this sample
        if str(sample_number) in list(df_hla['NumAtendimento']):
            if orientation.lower() == 'r':
                df_hla.set_value((df_hla[df_hla['NumAtendimento'] == sample_number].index), 'SequenciaR', str(record.seq))
                #df_hla.set_value((df_hla[df_hla['NumAtendimento'] == sample_number].index), 'SequenciaF', str(record.seq))
                df_hla.set_value((df_hla[df_hla['NumAtendimento'] == sample_number].index), 'AleloR', allele)

            elif orientation.lower() == 'f':
                df_hla.set_value((df_hla[df_hla['NumAtendimento'] == sample_number].index), 'SequenciaR', str(record.seq))
                #df_hla.set_value((df_hla[df_hla['NumAtendimento'] == sample_number].index), 'SequenciaF', str(record.seq))
                df_hla.set_value((df_hla[df_hla['NumAtendimento'] == sample_number].index), 'AleloF', allele)

        else:
            if orientation.lower() == 'f':
                df_hla.loc[len(df_hla)] = [sample_number, 
                   pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['NomePaciente'],
                   '', allele, '',
                   #pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['RESULTADO'],
                   sisgeno_number,
                   pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['DataEntrada'],
                   pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['CodigoExame'],
                   str(record.seq),'']
            elif orientation.lower() == 'r':
                df_hla.loc[len(df_hla)] = [sample_number, 
                   pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['NomePaciente'],
                   '',  '',allele,
                   #pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['RESULTADO'],
                   sisgeno_number,
                   pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['DataEntrada'],
                   pleres_df[pleres_df['NumAtendimento']==int(sample_number)].iloc[0]['CodigoExame'],
                   '', str(record.seq)]


        #if sample_number not in dict_seqs.keys():
        #print('>%s_%s\n%s\n' % (sisgeno_number, orientation, record.seq))
        
        if str(sisgeno_number) + '.fasta' not in os.listdir('.'):
            with open(str(sisgeno_number) + '.fasta', 'w') as current_fasta:
                current_fasta.write('>%s_%s\n%s\n' % (sisgeno_number, orientation, record.seq))
        else:
            with open(str(sisgeno_number) + '.fasta', 'a') as current_fasta:
                current_fasta.write('>%s_%s\n%s\n' % (sisgeno_number, orientation, record.seq))
    
    # Iterate over the dataframe genotyping based on alleles

    for index, row in df_hla.iterrows():

        print(row['NomePaciente'])
        print('Alelo R: %s' % (row['AleloR']))
        print('Alelo F: %s' % (row['AleloF']))
        
        if row['AleloF'].upper() != Seq(row['AleloR']).complement():
            df_hla.set_value(index, 'Resultado', 'Inconsistência entre sequencias F e R')
        elif row['AleloF'].upper() == 'T' and row['AleloR'].upper() == 'A':
            df_hla.set_value(index, 'Resultado', 'Ausente')

        elif row['AleloF'].upper() == 'G' and row['AleloR'].upper() == 'C':
            df_hla.set_value(index, 'Resultado', 'Presente')

        elif row['AleloF'].upper() == 'K' and row['AleloR'].upper() == 'M':
            df_hla.set_value(index, 'Resultado', 'Presente')


    df_hla.to_csv('hla_b57_%s.tsv' % (datetime.now().strftime("%Y-%m-%d")), sep='\t')

    return(df_hla)
            
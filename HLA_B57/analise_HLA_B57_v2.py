#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Author: Gabriel dos Santos Goncalves
# Sao Paulo, Brazil - march 2019

import os
import pandas as pd
import re
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.Alphabet import generic_dna
from datetime import datetime
import sys
import pymssql

def usage():
    print("""
                ==== Script de processamento de amostras HLA B57 ====

Utilização:

python3 analise_HLA_B57_v2.py <arquivo multifasta> 

""")

if len(sys.argv) != 2:
    usage()
    sys.exit()


def fetch_pleres_backup_database():
    status_connection = False

    try:
        conn = pymssql.connect(server='ec2-18-228-94-141.sa-east-1.compute.amazonaws.com',
                            user='cdg.gabriel', password='Genomas@2018')

        conn.autocommit(True)
        cursor = conn.cursor()

        cursor.execute("""
                        USE [Pleres-CentroDeGenomas];
                        SELECT
                        RIGHT('000000000000'+CAST(R_E.ID AS VARCHAR(10)),12) AS 'cod barras',
                        pro.id 'prontuario',
                        a.id_posto 'unid',
                        a.num_atendimento 'atend',
                        pla.str_nome 'Plano',
                        e.str_nome 'exame',
                        e.id 'id_exame',
                        p.str_nome 'paciente',
                        pf.enum_sexo 'sexo',
                        a.num_idade_anos 'idade',
                        a.dt_entrada 'data entrada',
                        ea.dt_entrega 'data de entrega',
                        a.str_codigo_controle 'controle'
                        FROM exame_atendimento ea
                        JOIN atendimento a on ea.id_atendimento = a.id
                        JOIN exame e on ea.id_exame = e.id
                        JOIN prontuario pro on a.id_prontuario = pro.id
                        JOIN pessoa p on pro.id_pessoa = p.id
                        JOIN pessoa_fisica PF ON Pro.id_pessoa = PF.ID
                        JOIN recipiente_exame_ATENDIMENTO RE ON RE.id_exame_atendimento = EA.ID
                        JOIN recipiente_exame R_E ON RE.id_recipiente_exame = R_E.ID
                        JOIN plano pla on ea.id_plano = pla.id
                        AND a.bol_excluido = 0 ORDER BY A.dt_entrada
                        """)
        query_result = cursor.fetchall()

        df_pleres_backup_columns = ('Cod_Barras','prontuario', 'Unidade', 'Atendimento', 
                                    'plano', 'Exame', 'id_exame', 'cliente', 'sexo', 
                                    'idade', 'data_entrada', 'data_entrega', 'controle')
        df_pleres_backup = pd.DataFrame(query_result, columns=df_pleres_backup_columns)
        status_connection = True
        return(df_pleres_backup, status_connection)
    

    # If there is a problem with the connection, order the user to provide 
    except:
        print("""Problemas de conexão com servidor do Pleres.
                 Checar estado do usuário com setor de infraestrutura.""")
        return(None, status_connection)



def processando_dados_HLA_B57(multifasta):
    # Check connection with Pleres backup database on AWS
    df_pleres, status_pleres_con  = fetch_pleres_backup_database()
    if status_pleres_con == False:
        return()

    # Extracting recipient number for each sample
    list_recip_numbers = []
    with open(multifasta, 'r') as fasta:
        for line in fasta.readlines():
            if line.startswith('>'):
                #print(line)
                rec_number = re.match('>(\d+)_', line).group(1)
                list_recip_numbers.append(rec_number)

    
    
    #list_zfill_numbers = list(set([n.zfill(12) for n in list_recip_numbers]))
    #filtered_df = df_pleres[df_pleres.Atendimento.isin(list_recip_numbers)]
    filtered_df = df_pleres
    
    # Set date
    date = datetime.now().strftime("%y%m%d")

    # Set Multifasta object
    records = list(SeqIO.parse(multifasta, "fasta"))
    
    # Create dataframe to receive information from samples
    df_hla = pd.DataFrame(columns=['NumAtendimento', 'NomePaciente',
        'Resultado', 'AleloF', 'AleloR', 'Sisgeno', 'DataEntrada',
        'DataEntrega', 'CodigoExame', 'SequenciaF', 'SequenciaR'])
    
    # Get general information about data and print header message
    list_names = []
    for name in [x.name for x in records]:
        sample_number_search = re.search('(.+)_HCP', name)
        try:
            sample_number = str(sample_number_search.group(1))
            list_names.append(sample_number)
        except:
            continue
    
    list_unique_names = set(list_names)
   
    print('Arquivo processado %s.' % (multifasta))
    print('Número total de sequências: %s' % (len(list_names)))
    print('Número total de amostras: %s' % (len(list_unique_names)))
    print('\nLog de problemas\n')
    
    dict_seqs={}
    for record in records:
        # Get sample number 
        sample_number_search = re.search('(.+)_HCP', record.name)
        #print(record.name)
        try:
            sample_number = str(sample_number_search.group(1))
        except:
            print('Sequência %s com padrão de número de recipiente inconsistente.' % (record.name))
            continue
        # Get primer orientation
        orientation_search = re.search('.+_HCP_(.)_', record.name)
        try:
            orientation = orientation_search.group(1)
        except:
            print('Orientação do primer da %s com padrão de inconsistente.' % (record.name))
            continue
        
        # Get Information from Pleres backup
        try:        
            cell = filtered_df[filtered_df['Atendimento']==int(sample_number)]
            sisgeno_number = cell.at[cell.index[0], 'controle']
            patient_name = cell.at[cell.index[0], 'cliente']
            service_number = cell.at[cell.index[0], 'Atendimento']
            start_date = cell.at[cell.index[0], 'data_entrada']
            delivery_date = cell.at[cell.index[0], 'data_entrega']
            exam_id = cell.at[cell.index[0], 'id_exame']
        
        except:            
            print('Problema em busca de informações para paciente %s.' % (record.name))
            continue
        
        # Get allele
        allele = ''
        if orientation.lower() == 'f':
            try:
                allele = re.search('TTCCCCTG(\w)', str(record.seq)).group(1)
            except:
                print('Problema em encontrar alelo alvo na sequência alvo da amostra %s' % (record.name))

        elif orientation.lower() == 'r':

            try:
                allele = re.search('AGCTGCC(\w)', str(record.seq)).group(1)
            except:
                print('Problema em encontrar alelo alvo na sequência alvo da amostra %s' % (record.name))
      
        
        # Check if df_hla already with information about this sample
        if str(sample_number) in list(df_hla['NumAtendimento']):
            if orientation.lower() == 'r':
                df_hla.at[(df_hla[df_hla['NumAtendimento'] == sample_number].index), 'SequenciaR'] = str(record.seq)
                df_hla.at[(df_hla[df_hla['NumAtendimento'] == sample_number].index), 'AleloR'] = allele

            elif orientation.lower() == 'f':
                df_hla.at[(df_hla[df_hla['NumAtendimento'] == sample_number].index), 'SequenciaR'] = str(record.seq)
                df_hla.at[(df_hla[df_hla['NumAtendimento'] == sample_number].index), 'AleloF'] = allele

        else:
            if orientation.lower() == 'f':
                df_hla.loc[len(df_hla)] = [sample_number, patient_name, '', allele, '', sisgeno_number,
                   start_date, delivery_date, exam_id, str(record.seq),'']
            elif orientation.lower() == 'r':
                df_hla.loc[len(df_hla)] = [sample_number, patient_name, '', '', allele, sisgeno_number,
                   start_date, delivery_date, exam_id, str(record.seq),'']


        if str(sisgeno_number) + '.fasta' not in os.listdir('.'):
            with open(str(sisgeno_number) + '.fasta', 'w') as current_fasta:
                current_fasta.write('>%s_%s\n%s\n' % (sisgeno_number, orientation, record.seq))
        else:
            with open(str(sisgeno_number) + '.fasta', 'a') as current_fasta:
                current_fasta.write('>%s_%s\n%s\n' % (sisgeno_number, orientation, record.seq))
    
    # Iterate over the dataframe genotyping based on alleles
    for index, row in df_hla.iterrows():

        print(row['NomePaciente'])
        print('Alelo R: %s' % (row['AleloR']))
        print('Alelo F: %s' % (row['AleloF']))
        
        if row['AleloF'].upper() != Seq(row['AleloR']).complement():
            df_hla.at[index, 'Resultado'] = 'Inconsistência entre sequencias F e R'
        elif row['AleloF'].upper() == 'T' and row['AleloR'].upper() == 'A':
            df_hla.at[index, 'Resultado'] = 'Ausente'

        elif row['AleloF'].upper() == 'G' and row['AleloR'].upper() == 'C':
            df_hla.at[index, 'Resultado'] = 'Presente'

        elif row['AleloF'].upper() == 'K' and row['AleloR'].upper() == 'M':
            df_hla.at[index, 'Resultado'] = 'Presente'


    df_hla.to_csv('hla_b57_%s.tsv' % (datetime.now().strftime("%Y-%m-%d")), sep='\t')

    return(df_hla)


processando_dados_HLA_B57(sys.argv[1])
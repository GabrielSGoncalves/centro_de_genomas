# Projeto HLA B*5701 Centro de Genomas
Este repositório descreve a utilização dos scripts para processamento dos dados provenientes do ensaio de [Sensibilidade ao Abacavir HLA-B 57*01](http://centrodegenomas.com.br/n/hla-b-5701/) ([rs2395029](https://www.snpedia.com/index.php/Rs2395029)).

Para que o usuário consiga processar os arquivos resultantes do ensaio, deve seguir os passos descritos a seguir:

**1)** Descrever passos de entrada no sequenciador 

**2)** Descrever passos de como trabalhar no Geneious

**3)** Descrever como exportar os arquivos do Geneious para um multifasta.

**4)** O usuário em seguida deverá abrir o programa [Anaconda](https://anaconda.org/) presente em seu sistema operacional e selecionar o item [**Jupyter Notebook**](http://jupyter.org/);

**5)** Com a aba do Jupyter aberto em seu navegador, ir até a sua pasta local XXXXXXX e selecionar o notebook **Notebook_HLA_B57_v1**;

**6)** O notebook irá descrever as etapas necessárias para executar o script de processamento dos dados, que consistem basicamente importar o módulo de análise (**'analise_HLA_B57_v1'**), ir até a pasta aonde estão os seus arquivos e rodar a função **'processando_dados_HLA_B57'**.

**7)** Para a função **'processando_dados_HLA_B57'** ser executadas de forma satisfatória, o usuário dever fornecer como parâmetros o nome de dois arquivos:
   
   * FASTA - um arquivo multifasta com todas as sequências foward e reverse das amostras 
   * Tabela de informação Pleres -  um arquivo XLSX (Excel) com as informações baixadas do Pleres

**8)** Durante a execução da função **'processando_dados_HLA_B57'**, será impresso na tela as sequências fasta já com o nome do Sisgeno respectivo. Como arquivos de saída, o usuário terá acesso à um fasta para cada amostra com as sequências F e R, além de uma tabela TSV com diversas informações de cada amostra processada, inclusive o resultado da genotipagem.




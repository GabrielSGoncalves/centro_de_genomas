library("stringr")
options(scipen=999)

snps=read.table("References/IAD138495_184_painel_4P_genomica.hotspot_fixed2.bed",sep="\t")

#vcf_alvo=read.table("~/Scripts/snps_4p_nutri/References/R_2019_01_24_13_26_33_user_S5-0476-68-Painel_4P_chip22.txt",sep="\t",header=TRUE)
#vcf_alvo=vcf_alvo[grepl("483714",vcf_alvo[,47]) & grepl("Hotspot",vcf_alvo$Allele.Source),]
#hotspot=str_split(vcf_alvo[,51],pattern=":")
#hotspot=do.call(rbind,hotspot)
#hotspot=data.frame(Chr=gsub("r0","r",hotspot[,1]),Start=as.numeric(hotspot[,2])-400,End=as.numeric(hotspot[,2])+400,Name=paste(vcf_alvo$Allele.Name,vcf_alvo$Chrom,vcf_alvo$Position,sep="_"))
hotspot=snps
hotspot=data.frame(Chr=hotspot[,1],Start=as.numeric(hotspot[,3])-400,End=as.numeric(hotspot[,3])+400,Name=paste(hotspot[,4],hotspot[,1],hotspot[,3],sep="_"))

system("mkdir -p analysis/4p_reference/")
write.table(hotspot,file="analysis/4p_reference/4P_regions.bed",sep="\t",col.names=FALSE,row.names=FALSE,quote=FALSE)


print("sort, merged, complement")
system("bedtools sort -i analysis/4p_reference/4P_regions.bed -faidx References/ucsc_hg19_chr_name.txt | bedtools merge | bedtools complement -g References/ucsc_hg19_chr_size.txt -i - > analysis/4p_reference/regions_to_mask.bed")

print("masking")
system("bedtools maskfasta -fi ~/References/ucsc_hg19.fa -fo analysis/4p_reference/ucsc_hg19_masked_4p.fasta -bed analysis/4p_reference/regions_to_mask.bed")

print("creating index")
system("bwa index analysis/4p_reference/ucsc_hg19_masked_4p.fasta")

print("indexing")
system("samtools faidx analysis/4p_reference/ucsc_hg19_masked_4p.fasta")

print("creating dictionary")
system("gatk CreateSequenceDictionary -R analysis/4p_reference/ucsc_hg19_masked_4p.fasta -O analysis/4p_reference/ucsc_hg19_masked_4p.dict")

print("running gatk4 local")
system("bash scripts/pipe_gatk4_local.sh")

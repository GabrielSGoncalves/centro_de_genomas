
sample_folder="/home/gustavofernandes/Scripts/snps_4p_nutri";
sample_name="teste_486157";
exam="Nutri_4P";
reference="/home/gustavofernandes/Scripts/snps_4p_nutri/analysis/4p_reference/ucsc_hg19_masked_4p.fasta"


rm $sample_folder/analysis/*

for sample_name in `ls $sample_folder/raw/ | grep fastq`;
do
#rm $sample_folder/analysis/*
	#sample_name2=`perl -ple 's/^0*//gi' $sample_name | perl -ple 's/_.*//gi'`
    sample_name2=`echo $sample_name | perl -ple 's/^0*//gi'| perl -ple 's/_.*//gi'`;
        echo -e "\e[32m Sample: $sample_name \e[0m"
	echo -e "\e[33malingning fastq\e[0m"
	bwa mem -R @RG\\tID:$sample_name\\tSM:$sample_name\\tLB:$exam\\tPL:ion $reference "$sample_folder"/raw/"$sample_name" | samtools sort | samtools view -Sb > "$sample_folder"/analysis/"$sample_name2"_MD.bam

#        bwa bwasw $reference "$sample_folder"/raw/"$sample_name"_R1.fastq | samtools sort | samtools view -Sb > "$sample_folder"/analysis/"$sample_name"_MD.bam

        echo -e "\e[32m Sample: $sample_name \e[0m"
	echo -e "\e[33mmarking duplicates;\e[0m"
#	gatk MarkDuplicates -I "$sample_folder"/analysis/"$sample_name".bam -O "$sample_folder"/analysis/"$sample_name"_MD.bam -M "$sample_folder"/analysis/MD_metrics.txt 
        echo -e "\e[32m Sample: $sample_name \e[0m"
	echo -e "\e[33mEstimating Base recalibration parameters;\e[0m"
	gatk4.0 BaseRecalibrator -I "$sample_folder"/analysis/"$sample_name2"_MD.bam -R $reference -O "$sample_folder"/analysis/base_recal.txt --known-sites ~/References/known_sites/dbsnp_138.hg19.vcf --known-sites ~/References/known_sites/dbsnp_138.hg19.excluding_sites_after_129.vcf

        echo -e "\e[32m Sample: $sample_name \e[0m"
	echo -e "\e[33mRecablibrating bases;\e[0m"
	gatk4.0 ApplyBQSR -I "$sample_folder"/analysis/"$sample_name2"_MD.bam -bqsr "$sample_folder"/analysis/base_recal.txt -O "$sample_folder"/analysis/"$sample_name2"_MD_BR.bam
        echo -e "\e[32m Sample: $sample_name \e[0m"
	echo -e "\e[33mCalling variants;\e[0m"
	gatk4.0 HaplotypeCaller -I "$sample_folder"/analysis/"$sample_name2"_MD_BR.bam -R $reference -O "$sample_folder"/analysis/"$sample_name2".vcf -D ~/References/known_sites/dbsnp_138.hg19.vcf  -ERC BP_RESOLUTION -L bed_intervals_test.intervals  #-DF NotDuplicateReadFilter --output-mode EMIT_ALL_SITES -L bed_intervals_test.intervals
done

#gatk4.0 GenotypeGVCFs

import os
import shutil
import pandas as pd
import re
import bs4
import pdfkit
import pymssql
from datetime import datetime
from PyPDF2 import PdfFileReader, PdfFileWriter, PdfFileMerger

# Connect to Pleres server
try:
    conn = pymssql.connect(server="192.168.0.34",
                           user="GABRIEL", password="gabriel!@")
    conn.autocommit(True)
    cursor = conn.cursor()
except:
    print("Não há conexão com banco de dados do Pleres.")
    cursor = None


# Function to access data from patient on Pleres database
def get_patient_data_from_pleres(acc_nummber):
    # Set the columns for dataframe
    list_columns_pleres = ['Cod_Barras', 'prontuario', 'Unidade', 'Atendimento', 'Exame',
                           'cliente', 'sexo', 'idade', 'Data_Entrada']

    # Fetch the information on the pleres database
    cursor.execute("""SELECT a.amostra as Cod_Barras, a.prontuario, substring(b.amostra11,1,3)
                     as Unidade, substring(b.amostra11,5,7) as Atendimento,
                     b.codigo as Exame, a.cliente, a.sexo, a.idade, a.dataentra as Data_Entrada
                     FROM CG.DBO.pac a INNER JOIN CG.DBO.exa b on a.amostra=b.amostra""")
    query_result = cursor.fetchall()

    # Generate dataframe out of the result from the sql query
    df = pd.DataFrame(query_result, columns=list_columns_pleres)

    # Rstrip the values from the columns
    df['Exame'] = df['Exame'].apply(lambda x: x.rstrip())
    df['Cod_Barras'] = df['Cod_Barras'].apply(lambda x: x.rstrip())
    df['sexo'] = df['sexo'].apply(lambda x: x.rstrip())

    #
    df = df[df['Cod_Barras'] == acc_nummber.zfill(12)]
    # Change the data types from columns
    return(df)

rs_to_genename = {'rs492842': 'rs492842 (BHMT)', 'rs4402960':
                  'rs4402960 (IGF2BP2)', 'rs1801198': 'rs1801198 (TCN2)',
                  'rs17300539': 'rs17300539 (ADIPQ)', 'rs8050136': 'rs8050136 (FTO)',
                  'rs7501331': 'rs7501331 (BCMO1)', 'rs2282679': 'rs2282679 (GC)',
                  'rs4988235': 'rs4988235 (MCM6)', 'rs4639334': 'rs4639334 (HLA)',
                  'rs429358': 'rs429358 (APOE)', 'rs4680': 'rs4680 (COMT)',
                  'rs1051740': 'rs1051740 (EPHX1)', 'rs1801282': 'rs1801282 (PPARG)',
                  'rs6756629': 'rs6756629 (ABCG5)', 'rs174546': 'rs174546 (FADS1)',
                  'rs5128': 'rs5128 (APOC3)', 'rs1805094': 'rs1805094 (LEPR)',
                  'rs1051266': 'rs1051266 (RFC-SLC19A1)', 'rs5082': 'rs5082 (APOA2)',
                  'rs7412': 'rs7412 (APOE)', 'rs7498665': 'rs7498665 (SH2B1)',
                  'rs1800629': 'rs1800629 (TNF-alfa)', 'rs2187668': 'rs2187668 (HLA)',
                  'rs28932472': 'rs28932472 (POMC)', 'rs4654748': 'rs4654748 (NBPF3)',
                  'rs894160': 'rs894160 (PLIN)', 'rs7647305': 'rs7647305 (ETV5)',
                  'rs10830963': 'rs10830963 (MTNR1B)', 'rs953413': 'rs953413 (ELOVL2)',
                  'rs1049434': 'rs1049434 (MCT-1)', 'rs74837985': 'rs74837985 (GSTM1 del)',
                  'rs708272': 'rs708272 (CETP)', 'rs4880': 'rs4880 (SOD2)',
                  'rs3846662': 'rs3846662 (HMGCR)', 'rs7566605': 'rs7566605 (INSIG2)',
                  'rs1042713': 'rs1042713 (ADBR2)', 'rs1800592': 'rs1800592 (UCP1)',
                  'rs13702': 'rs13702 (LPL)', 'rs17782313': 'rs17782313 (MC4R)',
                  'rs1042714': 'rs1042714 (ADBR2)', 'rs5443': 'rs5443 (GNB3)',
                  'rs6721961': 'rs6721961 (NRF2)', 'rs2395182': 'rs2395182 (HLA)',
                  'rs174537': 'rs174537 (MYRF)', 'rs2228570': 'rs2228570 (VDRFokI)',
                  'rs602662': 'rs602662 (FUT2)', 'rs2070895': 'rs2070895 (LIPC)',
                  'rs13266634': 'rs13266634 (SLC30A8)', 'rs1801394': 'rs1801394 (MTRR)',
                  'rs11206510': 'rs11206510 (PCSK9)', 'rs2266633': 'rs2266633 (GSTT1 del)',
                  'rs17602729': 'rs17602729 (AMPD1)', 'rs5219': 'rs5219 (KCNJ11)',
                  'rs234706': 'rs234706 (CBS)', 'rs9939609': 'rs9939609 (FTO)',
                  'rs7799039': 'rs7799039 (LEP)', 'rs2071487': 'rs2071487 (GSTM1 del)',
                  'rs7775228': 'rs7775228 (HLA)', 'rs1800795': 'rs1800795 (IL6)',
                  'rs33972313': 'rs33972313 (SLC23A1)', 'rs925946': 'rs925946 (BDNF)',
                  'rs7903146': 'rs7903146 (TCF7L2)', 'rs2016520': 'rs2016520 (PPAR-delta)',
                  'rs731236': 'rs731236 (VDRTaqI)', 'rs6234': 'rs6234 (PCKS1)',
                  'rs8192678': 'rs8192678 (PGC1-alfa)', 'rs8111989': 'rs8111989 (CKMM)',
                  'rs1050450': 'rs1050450 (GPX1)', 'rs1001179': 'rs1001179 (CAT)',
                  'rs4299376': 'rs4299376 (ABCG8)', 'rs2266637': 'rs2266637 (GSTT1 del)',
                  'rs1138272': 'rs1138272 (GSTP1)', 'rs659366': 'rs659366 (UCP2)',
                  'rs964184': 'rs964184 (BUD13)', 'rs6813195': 'rs6813195 (TMEM154)',
                  'rs4986790': 'rs4986790 (TLR4)', 'rs1800566': 'rs1800566 (NQO1)',
                  'rs1799883': 'rs1799883 (FABP2)', 'rs5925': 'rs5925 (LDLR)',
                  'rs2229616': 'rs2229616 (MC4R)', 'rs1805087': 'rs1805087 (MTR)',
                  'rs1800588': 'rs1800588 (LIPC)', 'rs1799983': 'rs1799983 (eNOS)',
                  'rs688': 'rs688 (LDLR)', 'rs1695': 'rs1695 (GSTP1)',
                  'rs2943641': 'rs2943641 (IRS1)', 'rs662799': 'rs662799 (APOA5)',
                  'rs10938397': 'rs10938397 (GNPDA2)', 'rs1801260': 'rs1801260 (CLOCK)',
                  'rs361525': 'rs361525 (TNF-alfa)', 'rs1801131': 'rs1801131 (MTHFR)',
                  'rs12970134': 'rs12970134 (MC4R)', 'rs693': 'rs693 (APOB)',
                  'rs1801133': 'rs1801133 (MTHFR)', 'rs7454108': 'rs7454108 (HLA)',
                  'rs762551': 'rs762551 (CYP1A2)'}

dict_obesity_table = {'rs9939609': ['FTO (T>A)', 'A'],
                      'rs8050136': ['FTO (C>A)', 'A'],
                      'rs1042713': ['ADRB2 (A>G)', 'A'],
                      'rs1042714': ['ADRB2 (C>G)', 'G'],
                      'rs7799039': ['LEP (G>A)', 'A'],
                      'rs1805094': ['LEPR (G>C)', 'G'],
                      'rs17782313': ['MC4R (T>C)', 'C'],
                      'rs12970134': ['MC4R (G>A)', 'A'],
                      'rs2229616': ['MC4R (G>A)', 'G'],
                      'rs1801282': ['PPARG (C>G)', 'G'],
                      'rs925946': ['BDNF (T>G)', 'G'],
                      'rs7498665': ['SH2B1 (A>G)', 'G'],
                      'rs7566605': ['INSIG2 (C>G)', 'C'],
                      'rs894160': ['PLIN (G>A)', 'A'],
                      'rs7647305': ['ETV5 (T>C)', 'C'],
                      'rs5443': ['GNB3 (C>T)', 'T'],
                      'rs10938397': ['GNPDA2 (A>G)', 'G'],
                      'rs659366': ['UCP2 (G>A)', 'A'],
                      'rs17300539': ['ADIPOQ (G>A)', 'A'],
                      'rs28932472': ['POMC (C>G)', 'C'],
                      'rs5082': ['APOA2 (C>T)', 'C'],
                      'rs6234': ['PCSK1 (C>G)', 'G']}

dict_diabetes_table = {'rs1801282': ['PPARG (C>G)', 'C'],
                       'rs7903146': ['TCF7L2 (C>T)', 'T'],
                       'rs5219': ['KCNJ11 (T>C)', 'T'],
                       'rs13266634': ['SLC30A8 (C>T)', 'C'],
                       'rs4402960': ['IGF2BP2 (G>T)', 'T'],
                       'rs1800795': ['IL6 (G>C)', 'G'],
                       'rs10938397': ['GNPDA2 (A>G)', 'G'],
                       'rs2943641': ['IRS1 (T>C)', 'T'],
                       'rs6813195': ['TMEM154 (T>C)', 'C'],
                       'rs10830963': ['MTNR1B (C>G)', 'G']}

dict_folatecycle_table = {'rs1801133': ['MTHFR (C>T)', 'T'],
                          'rs1801131': ['MTHFR (A>C)', 'C'],
                          'rs1805087': ['MTR (G>A)', 'A'],
                          'rs1801394': ['MTRR (A>G)', 'G'],
                          'rs234706': ['CBS (A>G)', 'G'],
                          'rs1801198': ['TCN2 (C>G)', 'G'],
                          'rs4680': ['COMT (G>A)', 'A'],
                          'rs1051266': ['SLC19A1 (G>A)', 'A']}

dict_vitaminD_table = {'rs2228570': ['VDR Fok I (C>T)', 'T'],
                       'rs731236': ['VDR Taq I (T>C)', 'C'],
                       'rs2282679': ['GC', 'G']}

dict_lactase_table = {'rs4988235': ['MCM6 (C>T)', 'C']}

dict_caffeine_table = {'rs762551': ['CYP1A2 (C>A)', 'C']}

dict_vitamins_table = {'rs7501331': ['BCMO1 (C>T)', 'T'],
                       'rs964184': ['BUD13 (C>G)', 'C'],
                       'rs1801133': ['MTHFR (C>T)', 'T'],
                       'rs1801131': ['MTHFR (A>C)', 'C'],
                       'rs1805087': ['MTR (G>A)', 'A'],
                       'rs1801394': ['MTRR (A>G)', 'G'],
                       'rs234706': ['CBS (A>G)', 'G'],
                       'rs1801198': ['TCN2 (C>G)', 'G'],
                       'rs4680': ['COMT (G>A)', 'A'],
                       'rs33972313': ['SLC23A1 (C>T)', 'T'],
                       'rs4654748': ['NBPF3 (C>T)', 'C'],
                       'rs602662': ['FUT2 (A>G)', 'G'],
                       'rs1051266': ['SLC19A1 (G>A)', 'A']}

dict_stress_table = {'rs4880': ['SOD2 (T>C)', 'C'],
                     'rs1050450': ['GPX1 (C>T)', 'T'],
                     'rs1001179': ['CAT (A>G)', 'A'],
                     'rs2071487': ['GSTM1 del', 'DEL'],
                     'rs74837985': ['GSTM1 del', 'DEL'],
                     'rs1695': ['GSTP1 (A>G)', 'G'],
                     'rs1138272': ['GSTP1 (C>T)', 'T'],
                     'rs2266633': ['GSTT1 del', 'DEL'],
                     'rs2266637': ['GSTT1 del', 'DEL'],
                     'rs1800566': ['NQO1 (C>T)', 'T'],
                     'rs1800629': ['TNF-alfa (G>A)', 'A'],
                     'rs361525': ['TNF-alfa (G>A)', 'A'],
                     'rs1800795': ['IL6 (G>C)', 'G'],
                     'rs1051740': ['EPHX1 (T>C)', 'C'],
                     'rs6721961': ['NRF2 (C>A)', 'A'],
                     'rs4986790': ['TLR4 (A>G)', 'G']}


dict_bioenergetic_table = {'rs8192678': ['PGC1-alfa (G>A)', 'A'],
                           'rs2016520': ['PPAR-delta (C>T)', 'T'],
                           'rs1799983': ['ENOS (G>T)', 'T'],
                           'rs1049434': ['SLC16A1 (T>A)', 'A'],
                           'rs1801260': ['CLOCK (T>C)', 'C'],
                           'rs17602729': ['AMPD1 (C>T)', 'T'],
                           'rs8111989': ['CKMM (A>G)', 'G'],
                           'rs1800592': ['UCP1 (A>G)', 'A']}

dict_lipids_table = {'rs1799837': ['APOA1 (G>A)*', 'A'],
                     'rs662799': ['APOA5 (T>C)', 'C'],
                     'rs5128': ['APOC3 (C>G)', 'G'],
                     'rs693': ['APOB (C>T)', 'T'],
                     'rs688': ['LDLR (C>T)*', 'T'],
                     'rs5925': ['LDLR (C>T)', 'C'],
                     'rs708272': ['CETP (G>A)', 'G'],
                     'rs6756629': ['ABCG5 (A/C/G)',	'G'],
                     'rs4299376': ['ABCG8 (G>T)', 'G'],
                     'rs3846662': ['HMGCR (C>T)', 'T'],
                     'rs2070895': ['LIPC (A>G)', 'A'],
                     'rs1800588': ['LIPC (C>T)', 'C'],
                     'rs13702': ['LPL (A>G)', 'A'],
                     'rs964184': ['BUD13 (C>G)', 'G'],
                     'rs11206510': ['PCSK9 (G>A)', 'A'],
                     'rs174546': ['FADS1 (G>A)', 'A'],
                     'rs174537': ['MYRF (G>T)', 'G'],
                     'rs953413': ['ELOVL2 (C>T)', 'T']}

dict_rs429358 = {'TT': 'E2', 'CC': 'E4', 'TC': 'E3', 'CT': 'E3'}

dict_rs7412 = {'TT': 'E2', 'CC': 'E4', 'TC': 'E3', 'CT': 'E3'}

celiac_rs_list = ['rs2187668', 'rs7454108',
                  'rs2395182', 'rs7775228', 'rs4639334']

dict_systemic_arterical_hypertension = {'rs1799752':['ACE/ECA', 'del']}


def processing_nutrigenetics(csv_file, table_texts, gender='M'):
    global dict_obesity_table, rs_to_genename, dict_diabetes_table, dict_vitaminD_table

    # Define the path to the pictures
    red_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha vermelha.jpg'alt='Image not found'>"
    yellow_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha amarela.jpg'alt='Image not found'>"
    green_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha verde.jpg'alt='Image not found'>"

    # Dict for converting 'balls' into colors
    dict_balls_to_colors = {
        red_ball: 'red_ball',
        yellow_ball: 'yellow_ball',
        green_ball: 'green_ball'}

    # Set HTML head to use UTF-8
    html_head =  '''<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="/home/gabriel/Documents/Repos/centro_de_genomas/css_models/tables_style.css">
      </head>
      '''
    
    patient_header = '''
                <p>Nome: %s</p>
                <p>Idade: %s</p>
                <p>Número de recipiente: %s</p>
                <p>Número de atendimento: %s</p>
                <p>Número de prontuário: %s</p>
                <p>Exame: %s</p>
                    '''

    html_title = '''
          <h2>%s</h2>
    '''

    # Let pandas receive long string
    pd.set_option('display.max_colwidth', -1)

    # Removing the Meta data from raw data and loading the table as dataframe
    with open(csv_file, 'r') as filein:
        line_n = 0
        for line in filein.readlines():
            if line[:6] != "Sample":
                line_n += 1
            else:
                break

    # Try to open csv
    file_error = False
    try:
        df = pd.read_csv(csv_file, sep=';')
    except IOError:
        print("Arquivo %s não encontrado." % csv_file)
        file_error = True
    # If csv_file was correctly loaded
    if file_error == False:
        df = pd.read_csv(csv_file, sep='\t', skiprows=list(range(0, line_n)))

    # Create folder to receive the outputs
    output_folder_name = 'nutrigenetica_' + datetime.now().strftime("%Y-%m-%d")
    if output_folder_name not in os.listdir(os.getcwd()):
        os.mkdir(output_folder_name)

    # Load csv file with texts into dataframe
    df_texts = pd.read_csv(table_texts, sep=';')

    # Iterate over the dataframe
    for index, row in df.iterrows():

        # Create dataframes
        dataframe_obesity = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_lipids = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_diabetes = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_systemic_arterical_hypertension = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_folatecycle = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_vitaminD = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_lactase = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_celiac = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_caffeine = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_stress = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_vitamins = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        dataframe_bioenergetic = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'RESULTADO', 'ANÁLISE'])
        
        # Sample name
        sample_number = str(row['Sample/Assay'])

        # Create folder to receive files from specific sample
        if sample_number not in os.listdir(os.getcwd()):
            os.mkdir(sample_number)

        # Get patient gender from Pleres
        try:
            patient_info_df = get_patient_data_from_pleres(sample_number.zfill(12))
            patient_name = str(patient_info_df.iloc[0]['cliente'])
            patient_age = str(patient_info_df.iloc[0]['idade'])
            recipient_number = str(patient_info_df.iloc[0]['Unidade']) + '-' + str(patient_info_df.iloc[0]['Atendimento'])
            report_number = str(patient_info_df.iloc[0]['prontuario'])
            assay_name = str(patient_info_df.iloc[0]['Exame'])
            gender = str(patient_info_df.iloc[0]['sexo'])
        
        except:
            print("Impossibilidade de conexão com Pleres.")
            patient_name = ''
            patient_age = ''
            recipient_number = ''
            report_number = ''
            assay_name = ''
            gender = 'M'

        # Set patient header with data from Pleres
        patient_header_Pleres = patient_header % (patient_name, 
                                                  patient_age,
                                                  str(sample_number.zfill(12)),
                                                  recipient_number,
                                                  report_number,
                                                  assay_name,)

        # Parse data for dataframe_obesity
        for key, value in dict_obesity_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_obesity.loc[len(dataframe_obesity)] = [
                gene, dbsnp, risk, result, analysis]

        # Reorder the dataframe_obesity
        obesity_rs_order = ['rs9939609', 'rs8050136', 'rs1042713', 'rs1042714',
                            'rs7799039', 'rs1805094', 'rs17782313', 'rs12970134', 'rs2229616',
                            'rs1801282', 'rs925946', 'rs7498665', 'rs7566605', 'rs894160',
                            'rs7647305', 'rs5443', 'rs10938397', 'rs659366', 'rs17300539',
                            'rs28932472', 'rs5082', 'rs6234']

        dataframe_obesity['dbSNP_index'] = dataframe_obesity['dbSNP']
        dataframe_obesity = dataframe_obesity.set_index('dbSNP_index')
        dataframe_obesity = dataframe_obesity.reindex(obesity_rs_order)

        # Write obesity table dataframe to HTML
        obesity_title = str(html_title % ('Tabela Obesidade',))

        with open(str(row['Sample/Assay']) + "_obesidade_nutrigenetica.html", 'w') as obesity_html:
            obesity_html.write(html_head + patient_header_Pleres + obesity_title +
                               dataframe_obesity.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_obesidade_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_obesidade_nutrigenetica.pdf")

      
      # Parse data for dataframe_lipids
        for key, value in dict_lipids_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"

            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'
            
            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_lipids.loc[len(dataframe_lipids)] = [
                gene, dbsnp, risk, result, analysis]

        # Parse the variants related to APOE
        try:
          rs429358_result = row['rs429358 (APOE)'].replace('/', '')
          rs7412_result = row["rs7412 (APOE)"].replace('/', '')
        except:
          rs429358_result = str(row['rs429358 (APOE)'])
          rs7412_result = str(row["rs7412 (APOE)"])

        # Haplotypes decision tree based on the genotypes
        if rs429358_result == 'TT' and rs7412_result == 'TT':
            APOE_result = 'E2/E2'
            APOE_analysis = green_ball

        elif rs429358_result == 'TT' and rs7412_result == 'CC':
            APOE_result = 'E3/E3'
            APOE_analysis = yellow_ball

        elif rs429358_result == 'CC' and rs7412_result == 'CC':
            APOE_result = 'E4/E4'
            APOE_analysis = red_ball

        elif rs429358_result == 'CT' and rs7412_result == 'CC':
            APOE_result = 'E3/E4'
            APOE_analysis = red_ball

        elif rs429358_result == 'TT' and rs7412_result == 'CT':
            APOE_result = 'E2/E3'
            APOE_analysis = green_ball

        elif rs429358_result == 'CT' and rs7412_result == 'CT':
            APOE_result = 'E2/E4'
            APOE_analysis = red_ball

        else:
            APOE_result = 'NOCALL'
            APOE_analysis = 'NOCALL'

        # Define values for APOE
        APOE_dbsnp = 'rs429358 / rs7412'
        APOE_gene = 'APOE genótipo'
        APOE_risk = 'E4'

        # Write the values to dataframe
        dataframe_lipids.loc[len(dataframe_lipids)] = [
            APOE_gene, APOE_dbsnp, APOE_risk, APOE_result, APOE_analysis]
        dataframe_lipids.loc[len(dataframe_lipids)] = [
            'APOE (SNPs)', APOE_dbsnp, '-', rs429358_result + '/' + rs7412_result, APOE_analysis]

        # Parsing FABP2
        rs1799883_result = str(row['rs1799883 (FABP2)']).replace('/', '')
        FABP2_dbsnp = 'rs1799883'
        FABP2_gene = 'FABP2 (G>A)'
        
        print("\n\nPATIENT NAME: " + patient_name) 
        print("\n\nPATIENT GENDER: " + gender + "\n\n" + 20*'&') 
        # Checking patient gender
        if gender == "M":
            risk_FABP2 = 'A'
            # Setting the risk balls figs
            try:
                result_value = rs1799883_result.count(risk_FABP2)
            except:
                result_value = "error"
            print('RESULT VALUE:' + str(rs1799883_result) + '\t' + str(result_value))

        elif gender == "F":
            risk_FABP2 = 'G'
            # Setting the risk balls figs
            try:
                result_value = rs1799883_result.count(risk_FABP2)
            except:
                result_value = "error"
            print('RESULT VALUE:' + str(rs1799883_result) + '\t' + str(result_value))

        else:
            result_value = "error"
            print('RESULT VALUE:' + str(rs1799883_result) + '\t' + str(result_value))


        # Setting the results for FABP2
        if result_value == 2:
            analysis_rs1799883 = red_ball
        elif result_value == 1:
            analysis_rs1799883 = yellow_ball
        elif result_value == 0:
            analysis_rs1799883 = green_ball
        else:
            analysis_rs1799883 = 'error'

        try:
            rs1799883_result = rs1799883_result.replace('/', '')
        except:
            print('Problema removendo / de resultado')

        dataframe_lipids.loc[len(dataframe_lipids)] = [
            FABP2_gene, FABP2_dbsnp, risk_FABP2, rs1799883_result, analysis_rs1799883]
        
        # Reorder the dataframe_lipids
        lipids_gene_order =  ['APOA1 (G>A)*', 'APOA5 (T>C)', 'APOC3 (C>G)', 'APOB (C>T)',
                              'APOE (SNPs)', 'APOE genótipo', 'LDLR (C>T)*', 'LDLR (C>T)',
                              'CETP (G>A)', 'ABCG5 (A/C/G)', 'ABCG8 (G>T)','LIPC (A>G)',
                              'LIPC (C>T)', 'HMGCR (C>T)', 'LPL (A>G)', 'FABP2 (G>A)',
                              'PCSK9 (G>A)', 'FADS1 (G>A)', 'MYRF (G>T)', 'ELOVL2 (C>T)',
                              'BUD13 (C>G)']
        dataframe_lipids['GENE_index'] = dataframe_lipids['GENE']
        dataframe_lipids = dataframe_lipids.set_index('GENE_index')
        dataframe_lipids = dataframe_lipids.reindex(lipids_gene_order)
        
        # Write dataframe_lipids to HTML
        lipids_title = str(html_title % ('Tabela Lipídeos',))
        with open(str(row['Sample/Assay']) + "_lipideos_nutrigenetica.html", 'w') as lipideos_html:
            lipideos_html.write(
                html_head + patient_header_Pleres + lipids_title + dataframe_lipids.to_html(escape=False, index=False))
        
        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_lipideos_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_lipideos_nutrigenetica.pdf")

        # Parse data for dataframe_diabetes
        for key, value in dict_diabetes_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_diabetes.loc[len(dataframe_diabetes)] = [
                gene, dbsnp, risk, result, analysis]

        # Reorder the dataframe_diabetes
        diabetes_rs_order = ['rs1801282', 'rs7903146', 'rs5219', 'rs13266634',
                             'rs4402960', 'rs1800795', 'rs10938397', 'rs2943641', 'rs6813195', 'rs10830963']

        dataframe_diabetes['dbSNP_index'] = dataframe_diabetes['dbSNP']
        dataframe_diabetes = dataframe_diabetes.set_index('dbSNP_index')
        dataframe_diabetes = dataframe_diabetes.reindex(diabetes_rs_order)

        # Write diabetes table dataframe to HTML
        diabetes_title = str(html_title % ('Tabela Diabetes',))
        with open(str(row['Sample/Assay']) + "_diabetes_nutrigenetica.html", 'w') as diabetes_html:
            diabetes_html.write(html_head + patient_header_Pleres + diabetes_title +
                                dataframe_diabetes.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_diabetes_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_diabetes_nutrigenetica.pdf")
        

        # Parse data for dataframe_systemic_arterical_hypertension
        for key, value in dict_systemic_arterical_hypertension.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            analysis = ''

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')
            
            dataframe_systemic_arterical_hypertension.loc[len(dataframe_systemic_arterical_hypertension)] = [
                gene, dbsnp, risk, result, analysis]

        # Write dataframe_systemic_arterical_hypertension to HTML
        hypertension_title = str(html_title % ('Tabela de Hipertensão Arterial Sistêmica',))
        with open(str(row['Sample/Assay']) + "_hipertensao_arterial_nutrigenetica.html", 'w') as hypertension_html:
            hypertension_html.write(html_head + patient_header_Pleres + hypertension_title +
                              dataframe_systemic_arterical_hypertension.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_hipertensao_arterial_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_hipertensao_arterial_nutrigenetica.pdf")


        # Parse data for dataframe_folatecycle
        for key, value in dict_folatecycle_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_folatecycle.loc[len(dataframe_folatecycle)] = [
                gene, dbsnp, risk, result, analysis]

        # Reorder the dataframe_folatecycle
        folate_rs_order = ['rs1801133', 'rs1801131', 'rs1805087', 'rs1801394',
                           'rs234706', 'rs1801198', 'rs4680', 'rs1051266']

        dataframe_folatecycle['dbSNP_index'] = dataframe_folatecycle['dbSNP']
        dataframe_folatecycle = dataframe_folatecycle.set_index('dbSNP_index')
        dataframe_folatecycle = dataframe_folatecycle.reindex(folate_rs_order)

        # Write dataframe_folatecycle to HTML
        folatecycle_title = str(html_title % ('Tabela Ciclo do Folato',))
        with open(str(row['Sample/Assay']) + "_folato_nutrigenetica.html", 'w') as folate_html:
            folate_html.write(html_head + patient_header_Pleres + folatecycle_title +
                              dataframe_folatecycle.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_folato_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_folato_nutrigenetica.pdf")

        # Parse data for dataframe_vitaminD
        for key, value in dict_vitaminD_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_vitaminD.loc[len(dataframe_vitaminD)] = [
                gene, dbsnp, risk, result, analysis]

        # Reorder the dataframe_vitaminD
        vitaminD_rs_order = ['rs2228570', 'rs731236', 'rs2282679']

        dataframe_vitaminD['dbSNP_index'] = dataframe_vitaminD['dbSNP']
        dataframe_vitaminD = dataframe_vitaminD.set_index('dbSNP_index')
        dataframe_vitaminD = dataframe_vitaminD.reindex(vitaminD_rs_order)


        # Set the text for vitamin D based on analysis
        try:            
            genotype_FOK_I = dataframe_vitaminD[dataframe_vitaminD['GENE'] == 'VDR Fok I (C>T)'].iloc[0][3].replace('/','')
            genotype_GC = dataframe_vitaminD[dataframe_vitaminD['GENE'] == 'GC'].iloc[0][3].replace('/','')
            genotype_FOK_I_GC = genotype_FOK_I + '/' + genotype_GC

            table = 'Perfil de Vitamina D'
            query_vitaminD = "tabela == '{0}'".format(table)
            query_dataframe = df_texts.query(query_vitaminD)
            query_dataframe = query_dataframe[query_dataframe['resultado'].str.contains(genotype_FOK_I_GC)]
            
            conclusion = '\n<p>Conclusão: {0}</p>'.format(query_dataframe.iloc[0][4])
            sugestion = '\n<p>Sugestão: {0}</p>'.format(query_dataframe.iloc[0][5])
        except:
            flag_conclusion = 'Erro na busca por Conclusão, genótipo genotype_FOK_I_GC' 
            conclusion = '\n<p>Conclusão: {0}</p>'.format(flag_conclusion)
            sugestion = '\n<p>Sugestão: {0}</p>'.format('Erro na busca por Sugestão')

        #print(genotype_FOK_I_GC)
        #print(query_dataframe)
            
        # Write dataframe_vitaminD to HTML
        vitaminD_title = str(html_title % ('Tabela Vitamina D',))
        with open(str(row['Sample/Assay']) + "_vitaminaD_nutrigenetica.html", 'w') as vitaminD_html:
            vitaminD_html.write(html_head + patient_header_Pleres + vitaminD_title +
                                dataframe_vitaminD.to_html(escape=False, index=False) + 
                               conclusion + sugestion)

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_vitaminaD_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_vitaminaD_nutrigenetica.pdf")

        # Parse data for dataframe_lactase
        for key, value in dict_lactase_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'
            
            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_lactase.loc[len(dataframe_lactase)] = [
                gene, dbsnp, risk, result, analysis]

        # Set the text for lactose intolerance based on analysis
        table = 'Perfil de Lactase'
        query_lactase = "tabela == '%s' and analise == '%s'" % (table, dict_balls_to_colors[analysis])
        query_dataframe = df_texts.query(query_lactase)
        conclusion = '\n<p>Conclusão: %s</p>' %  (query_dataframe.iloc[0]['conclusao'],)
        sugestion = '\n<p>Sugestão: %s</p>' % (query_dataframe.iloc[0]['sugestao'],)

        # Write dataframe_lactase to HTML
        lactase_title = str(html_title % ('Tabela Lactose',))
        with open(str(row['Sample/Assay']) + "_lactase_nutrigenetica.html", 'w') as lactase_html:
            lactase_html.write(html_head + patient_header_Pleres + lactase_title +
                               dataframe_lactase.to_html(escape=False, index=False) + 
                               conclusion + sugestion )

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_lactase_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_lactase_nutrigenetica.pdf")

        # Parse data for dataframe_caffeine
        for key, value in dict_caffeine_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_caffeine.loc[len(dataframe_caffeine)] = [
                gene, dbsnp, risk, result, analysis]

        # Set the text for caffeine based on analysis
        table = 'Perfil de Cafeína'
        query_caffeine = "tabela == '%s' and analise == '%s'" % (table, dict_balls_to_colors[analysis])
        query_dataframe = df_texts.query(query_caffeine)
        conclusion = '\n<p>Conclusão: %s</p>' %  (query_dataframe.iloc[0]['conclusao'],)
        sugestion = '\n<p>Sugestão: %s</p>' % (query_dataframe.iloc[0]['sugestao'],)

        # Write dataframe_caffeine to HTML
        caffeine_title = str(html_title % ('Tabela Cafeína',))
        with open(str(row['Sample/Assay']) + "_cafeina_nutrigenetica.html", 'w') as caffeine_html:
            caffeine_html.write(html_head + patient_header_Pleres + caffeine_title +
                                dataframe_caffeine.to_html(escape=False, index=False) + 
                               conclusion + sugestion)

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_cafeina_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_cafeina_nutrigenetica.pdf")

        # Parse data for dataframe_celiac
        analysis = green_ball
        haplotype = 'Ausência de alelos de risco'
        for rs in celiac_rs_list:
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = rs
            gene = 'HLA'
            try:
                result = row[rs_to_genename[rs]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.replace('/', '')
            except:
                result_value = "error"

            if rs == 'rs2187668':
                if 'T' in result_value and analysis == green_ball:
                    analysis = red_ball
                    haplotype = 'HLA-DQ 2.5'
                    
            elif rs == 'rs7454108':
                if 'G' in result_value and analysis == green_ball:
                    analysis = yellow_ball
                    haplotype = 'HLA-DQ 8'

            elif rs == 'rs2395182':
                if 'T' in result_value and analysis == green_ball:
                    analysis = green_ball
                    haplotype_rs2395182 = 'HLA-DQ 2.2'

                    
            elif rs == 'rs7775228':
                if 'G' in result_value and haplotype_rs2395182 == 'HLA-DQ 2.2':
                    analysis = yellow_ball
                    haplotype = 'HLA-DQ 2.2'
                    
            elif rs == 'rs4639334' and analysis == green_ball:
                if 'A' in result_value:
                    analysis = yellow_ball
                    haplotype = 'HLA-DQ 7'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_celiac.loc[len(dataframe_celiac)] = [
                gene, dbsnp, '-', result, '']

        # Setting the results and HLA values to dataframe
        dataframe_celiac.set_value((dataframe_celiac[dataframe_celiac[
                                   'dbSNP'] == 'rs2395182'].index), 'ANÁLISE', analysis)
        dataframe_celiac.set_value((dataframe_celiac[dataframe_celiac[
                                   'dbSNP'] == 'rs4639334'].index), 'ANÁLISE', haplotype)

        # Reorder the dataframe_celiac
        celiac_rs_order = ['rs2187668','rs7454108','rs2395182','rs7775228','rs4639334']

        dataframe_celiac['dbSNP_index'] = dataframe_celiac['dbSNP']
        dataframe_celiac = dataframe_celiac.set_index('dbSNP_index')
        dataframe_celiac = dataframe_celiac.reindex(celiac_rs_order)

         # Set the text for Celiac disease based on analysis
        table = 'Perfil de Glúten - Gene HLA'
        query_celiac = "tabela == '%s' and analise == '%s'" % (table, haplotype)
        query_dataframe = df_texts.query(query_celiac)
        conclusion = '\n<p>Conclusão: %s</p>' %  (query_dataframe.iloc[0]['conclusao'],)
        sugestion = '\n<p>Sugestão: %s</p>' % (query_dataframe.iloc[0]['sugestao'],)

        # Write dataframe_celiac to HTML
        celiac_title = str(html_title % ('Tabela Perfil Celíaco',))
        with open(str(row['Sample/Assay']) + "_perfilceliaco_nutrigenetica.html", 'w') as celiac_html:
            celiac_html.write(html_head + patient_header_Pleres + celiac_title +
                              dataframe_celiac.to_html(escape=False, index=False) + 
                               conclusion + sugestion)

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_perfilceliaco_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_perfilceliaco_nutrigenetica.pdf")

        # Parse data for dataframe_stress
        for key, value in dict_stress_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = result

            if key == 'rs2071487':
                if result == 'NOAMP':
                    analysis = red_ball
                    result_except = 'DEL'

                elif result in ['T/T']:
                    analysis = green_ball
                    result_except = 'INS'
                    risk = 'INS'

            elif key == 'rs74837985':
                if result == 'NOAMP':
                    analysis = red_ball
                    result_except = 'DEL'
                elif result in ['C/C','G/G','C/G', 'G/C']:
                    analysis = green_ball
                    result_except = 'INS'
                    risk = 'INS'
            
            elif key == 'rs2266633':
                if result == 'NOAMP':
                    analysis = red_ball
                    result_except = 'DEL'
                elif result in ['C/C']:
                    analysis = green_ball
                    result_except = 'INS'
                    risk = 'INS'

            elif key == 'rs2266637':
                if result == 'NOAMP':
                    analysis = red_ball
                    result_except = 'DEL'
                elif result in ['C/C']:
                    analysis = green_ball
                    result_except = 'INS'
                    risk = 'INS'
            else:
                if result_value == 2:
                    analysis = red_ball
                elif result_value == 1:
                    analysis = yellow_ball
                elif result_value == 0:
                    analysis = green_ball
                else:
                    analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')


            if key in ['rs2071487', 'rs74837985', 'rs2266633', 'rs2266637']:
              dataframe_stress.loc[len(dataframe_stress)] = [gene, dbsnp, risk, result_except.replace('/', ''), analysis]
            else:
              dataframe_stress.loc[len(dataframe_stress)] = [gene, dbsnp, risk, result, analysis]


        # Reorder the dataframe_stress
        stress_rs_order = ['rs4880', 'rs1050450', 'rs1001179', 'rs2071487', 'rs74837985', 'rs1695',
                           'rs1138272', 'rs2266633', 'rs2266637', 'rs1800566', 'rs1800629', 'rs361525', 'rs1800795',
                           'rs1051740', 'rs6721961', 'rs4986790']

        dataframe_stress['dbSNP_index'] = dataframe_stress['dbSNP']
        dataframe_stress = dataframe_stress.set_index('dbSNP_index')
        dataframe_stress = dataframe_stress.reindex(stress_rs_order)


        # Write dataframe_stress to HTML
        stress_title = str(html_title % ('Tabela Perfil de Estresse',))
        with open(str(row['Sample/Assay']) + "_estresse_nutrigenetica.html", 'w') as stress_html:
            stress_html.write(html_head + patient_header_Pleres + stress_title +
                              dataframe_stress.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_estresse_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_estresse_nutrigenetica.pdf")

        # Parse data for dataframe_vitamins
        for key, value in dict_vitamins_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_vitamins.loc[len(dataframe_vitamins)] = [
                gene, dbsnp, risk, result, analysis]

        # Reorder the dataframe_vitamins
        vitamins_rs_order = ['rs7501331', 'rs964184', 'rs1801133', 'rs1801131', 'rs1805087', 'rs1801394',
                             'rs234706', 'rs1801198', 'rs4680', 'rs33972313', 'rs4654748', 'rs602662', 'rs1051266']

        dataframe_vitamins['dbSNP_index'] = dataframe_vitamins['dbSNP']
        dataframe_vitamins = dataframe_vitamins.set_index('dbSNP_index')
        dataframe_vitamins = dataframe_vitamins.reindex(vitamins_rs_order)

        # Write dataframe_vitamins to HTML
        vitamins_title = str(html_title % ('Tabela Vitaminas',))
        with open(str(row['Sample/Assay']) + "_vitaminas_nutrigenetica.html", 'w') as vitamins_html:
            vitamins_html.write(html_head + patient_header_Pleres + vitamins_title +
                                dataframe_vitamins.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_vitaminas_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_vitaminas_nutrigenetica.pdf")

        # Parse data for dataframe_bioenergetic
        for key, value in dict_bioenergetic_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            try:
                result = row[rs_to_genename[key]]
            except:
                result = "error"
            # Setting the risk balls figs
            try:
                result_value = result.split("/").count(risk)
            except:
                result_value = "error"

            if result_value == 2:
                analysis = red_ball
            elif result_value == 1:
                analysis = yellow_ball
            elif result_value == 0:
                analysis = green_ball
            else:
                analysis = 'error'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_bioenergetic.loc[len(dataframe_bioenergetic)] = [
                gene, dbsnp, risk, result, analysis]

        # Reorder the dataframe_bioenergetic
        bioenergetic_rs_order = ['rs8192678', 'rs2016520', 'rs1799983', 'rs1049434',
                                 'rs1801260', 'rs17602729', 'rs8111989', 'rs1800592']

        dataframe_bioenergetic['dbSNP_index'] = dataframe_bioenergetic['dbSNP']
        dataframe_bioenergetic = dataframe_bioenergetic.set_index(
            'dbSNP_index')
        dataframe_bioenergetic = dataframe_bioenergetic.reindex(
            bioenergetic_rs_order)

        # Write dataframe_bioenergetic to HTML
        bioenergetic_title = str(html_title % ('Tabela Bioenergética',))
        with open(str(row['Sample/Assay']) + "_bioenergetica_nutrigenetica.html", 'w') as bioenergetic_html:
            bioenergetic_html.write(html_head + patient_header_Pleres + bioenergetic_title +
                                    dataframe_bioenergetic.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(row['Sample/Assay']) + "_bioenergetica_nutrigenetica.html",
                         str(row['Sample/Assay']) + "_bioenergetica_nutrigenetica.pdf")

        
        # Create merger for the pdf
        merger = PdfFileMerger()

        # Load PDF tables
        pdfTable_obesity = PdfFileReader(
            open(str(row['Sample/Assay']) + "_obesidade_nutrigenetica.pdf", "rb"))
        pdfTable_diabetes = PdfFileReader(
            open(str(row['Sample/Assay']) + "_diabetes_nutrigenetica.pdf", "rb"))
        pdfTable_hypertension = PdfFileReader(
            open(str(row['Sample/Assay']) + "_hipertensao_arterial_nutrigenetica.pdf", "rb"))
        pdfTable_folate = PdfFileReader(
            open(str(row['Sample/Assay']) + "_folato_nutrigenetica.pdf", "rb"))
        pdfTable_vitaminD = PdfFileReader(
            open(str(row['Sample/Assay']) + "_vitaminaD_nutrigenetica.pdf", "rb"))
        pdfTable_lactase = PdfFileReader(
            open(str(row['Sample/Assay']) + "_lactase_nutrigenetica.pdf", "rb"))
        pdfTable_caffeine = PdfFileReader(
            open(str(row['Sample/Assay']) + "_cafeina_nutrigenetica.pdf", "rb"))
        pdfTable_bioenergetic = PdfFileReader(
            open(str(row['Sample/Assay']) + "_bioenergetica_nutrigenetica.pdf", "rb"))
        pdfTable_lipids = PdfFileReader(
            open(str(row['Sample/Assay']) + "_lipideos_nutrigenetica.pdf", "rb"))
        pdfTable_stress = PdfFileReader(
            open(str(row['Sample/Assay']) + "_estresse_nutrigenetica.pdf", "rb"))
        pdfTable_vitamins = PdfFileReader(
            open(str(row['Sample/Assay']) + "_vitaminas_nutrigenetica.pdf", "rb"))
        pdfTable_celiac = PdfFileReader(
            open(str(row['Sample/Assay']) + "_perfilceliaco_nutrigenetica.pdf", "rb"))

        # Append tables
        merger.append(pdfTable_obesity)
        merger.append(pdfTable_lipids)
        merger.append(pdfTable_diabetes)
        merger.append(pdfTable_hypertension)
        merger.append(pdfTable_folate)
        merger.append(pdfTable_vitaminD)
        merger.append(pdfTable_lactase)
        merger.append(pdfTable_celiac)
        merger.append(pdfTable_caffeine)
        merger.append(pdfTable_stress)
        merger.append(pdfTable_vitamins)
        merger.append(pdfTable_bioenergetic)

        # Merge files
        merger.write(str(row['Sample/Assay']) + '_' + 
          patient_name.rstrip().replace(' ', '_') + "_laudo.pdf")

        # Moving files to folder sample folder
        for item in os.listdir(os.getcwd()):
            if os.path.isfile(item) and sample_number in item:
                if 'laudo' not in item:
                    try:
                        shutil.move(item, os.getcwd() + "/" + sample_number)
                    except:
                        print(str(_file) + " já na pasta.")
                elif 'laudo' in item:
                    try:
                        shutil.move(item, output_folder_name)
                    except:
                        print(str(_file) + " já na pasta.")
        try:
            shutil.move(sample_number, output_folder_name)
        except:
            print('Erro ao mover ' + sample_number +
                  ' para ' + output_folder_name)

    return(dataframe_vitaminD)

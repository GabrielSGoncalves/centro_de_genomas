import os
import pandas as pd
import re
import pdfkit
import pymssql
from datetime import datetime
import shutil


# Connect to Pleres server
try:
    conn = pymssql.connect(server="192.168.0.34",
                           user="GABRIEL", password="gabriel!@")
    conn.autocommit(True)
    cursor = conn.cursor()
except:
    print("Não há conexão com banco de dados do Pleres.")
    cursor = None


# Function to access data from patient on Pleres database
def get_patient_data_from_pleres(acc_nummber):
    # Set the columns for dataframe
    list_columns_pleres = ['Cod_Barras', 'prontuario', 'Unidade', 'Atendimento', 'Exame',
                           'cliente', 'sexo', 'idade', 'Data_Entrada']

    # Fetch the information on the pleres database
    cursor.execute("""SELECT a.amostra as Cod_Barras, a.prontuario, substring(b.amostra11,1,3)
                     as Unidade, substring(b.amostra11,5,7) as Atendimento,
                     b.codigo as Exame, a.cliente, a.sexo, a.idade, a.dataentra as Data_Entrada
                     FROM CG.DBO.pac a INNER JOIN CG.DBO.exa b on a.amostra=b.amostra""")
    query_result = cursor.fetchall()

    # Generate dataframe out of the result from the sql query
    df = pd.DataFrame(query_result, columns=list_columns_pleres)

    # Rstrip the values from the columns
    df['Exame'] = df['Exame'].apply(lambda x: x.rstrip())
    df['Cod_Barras'] = df['Cod_Barras'].apply(lambda x: x.rstrip())
    df['sexo'] = df['sexo'].apply(lambda x: x.rstrip())

    #
    df = df[df['Cod_Barras'] == acc_nummber.zfill(12)]
    # Change the data types from columns
    return(df)

# Function to get risk from the information on reference and alternative
# alleles


def get_genotype_risk_alpha(result, risk, reference_allele, alternative_allele):
    analysis = ''

    if reference_allele == risk:
        if result == 'Homozygous':
            analysis = 'BAIXO'
            # result += '_' + str(reference_allele) * 2
            result = str(reference_allele) * 2
        elif result == 'Heterozygous':
            analysis = 'INTERMEDIARIO'
            # result += '_' + str(reference_allele) + str(alternative_allele)
            result = str(reference_allele) + str(alternative_allele)
        elif result == 'Absent':
            analysis = 'ALTO'
            # result += '_' + str(alternative_allele) * 2
            result = str(alternative_allele) * 2
        elif result == 'No call':
            analysis = 'No call'

    elif alternative_allele == risk:
        if result == 'Homozygous':
            analysis = 'alto'
            # result += '_' + str(alternative_allele) * 2
            result = str(alternative_allele) * 2
        elif result == 'Heterozygous':
            analysis = 'Intermediario'
            # result += '_' + str(reference_allele) + str(alternative_allele)
            result = str(reference_allele) + str(alternative_allele)
        elif result == 'Absent':
            analysis = 'baixo'
            # result += '_' + str(reference_allele) * 2
            result = str(reference_allele) * 2

        elif result == 'No call':
            analysis = 'No call'

    elif alternative_allele == 'ERROR' or reference_allele == 'ERROR':
        analysis = 'No data available'

    else:
        analysis = '???'
        result += '???'

    return(analysis, genotype)


def get_genotype_risk(result, risk, reference_allele, alternative_allele):
    analysis = ''
    genotype = ''

    # Get the genotype
    if result == 'Homozygous':
        genotype = str(alternative_allele) * 2

    elif result == 'Absent':
        genotype = str(reference_allele) * 2

    elif result == 'Heterozygous':
        genotype = str(reference_allele) + str(alternative_allele)

    risk_count = genotype.count(risk)

    if risk_count == 2:
        analysis = 'ALTO'
    elif risk_count == 1:
        analysis = 'MEDIO'
    elif risk_count == 0:
        analysis = 'BAIXO'
    elif genotype == '':
        analysis = 'ERROR'

    if result == 'No call':
        analysis = 'No call'

    if alternative_allele == 'ERROR' or reference_allele == 'ERROR':
        analysis = 'No data available'

    return(analysis, genotype)


def processing_4P(tsv_file):
    # Information tables
    dict_obesity_table = {'rs9939609': ['FTO (T>A)', 'A'],
                          'rs8050136': ['FTO (C>A)', 'A'],
                          'rs1042713': ['ADRB2 (A>G)', 'A'],
                          'rs1042714': ['ADRB2 (C>G)', 'G'],
                          'rs7799039': ['LEP (G>A)', 'A'],
                          'rs1805094': ['LEPR (G>C)', 'G'],
                          'rs17782313': ['MC4R (T>C)', 'C'],
                          'rs12970134': ['MC4R (G>A)', 'A'],
                          'rs2229616': ['MC4R (C>T)', 'C'],
                          'rs1801282': ['PPARG (C>G)', 'G'],
                          'rs925946': ['BDNF (T>G)', 'G'],
                          'rs7498665': ['SH2B1 (A>G)', 'G'],
                          'rs7566605': ['INSIG2 (C>G)', 'C'],
                          'rs894160': ['PLIN (C>T)', 'T'],
                          'rs7647305': ['ETV5 (T>C)', 'C'],
                          'rs5443': ['GNB3 (C>T)', 'T'],
                          'rs10938397': ['GNPDA2 (A>G)', 'G'],
                          'rs659366': ['UCP2 (C>T)', 'T'],
                          'rs17300539': ['ADIPOQ (G>A)', 'A'],
                          'rs28932472': ['POMC (C>G)', 'C'],
                          'rs5082': ['APOA2 (G>A)', 'G']}

    dict_lipids_table = {'rs1799837': ['APOA1 (C>T)*', 'T'],
                         'rs662799': ['APOA5 (G>A)', 'G'],
                         'rs5128': ['APOC3 (C>G)', 'G'],
                         'rs693': ['APOB (G>A)', 'A'],
                         'rs688': ['LDLR (C>T)*', 'T'],
                         'rs5925': ['LDLR (C>T)', 'C'],
                         'rs708272': ['CETP (G>A)', 'G'],
                         'rs6756629': ['ABCG5 (A/C/G)', 'G'],
                         'rs4299376': ['ABCG8 (G>T)', 'G'],
                         'rs3846662': ['HMGCR (A>G)', 'A'],
                         'rs2070895': ['LIPC (A>G)', 'A'],
                         'rs1800588': ['LIPC (C>T)', 'C'],
                         'rs13702': ['LPL (T>C)', 'T'],
                         'rs964184': ['BUD13 (C>G)', 'G'],
                         'rs11206510': ['PCSK9 (T>C)', 'T'],
                         'rs174546': ['FADS1 (C>T)', 'T'],
                         'rs174537': ['MYRF (G>T)', 'G'],
                         'rs174616': ['FADS2 (A>G)', 'A'],
                         'rs953413': ['ELOVL2 (G>A)', 'A']}

    dict_diabetes_table = {'rs1801282': ['PPARG (C>G)', 'C'],
                           'rs7903146': ['TCF7L2 (C>T)', 'T'],
                           'rs5219': ['KCNJ11 (T>C)', 'T'],
                           'rs13266634': ['SLC30A8 (C>T)', 'C'],
                           'rs4402960': ['IGF2BP2 (G>T)', 'T'],
                           'rs1800795': ['IL6 (G>C)', 'G'],
                           'rs10938397': ['GNPDA2 (A>G)', 'G'],
                           'rs2943641': ['IRS1 (T>C)', 'T'],
                           'rs10830963': ['MTNR1B (C>G)', 'G'],
                           'rs6234': ['PCSK1 (C>G)', 'G']}

    dict_systemic_arterical_hypertension = {
        'rs1799998': ['CYP11B2 (A>G)', 'A']}

    dict_folatecycle_table = {'rs1801133': ['MTHFR (G>A)', 'A'],
                              'rs1801131': ['MTHFR (T>G)', 'G'],
                              'rs1805087': ['MTR (G>A)', 'A'],
                              'rs1801394': ['MTRR (A>G)', 'G'],
                              'rs234706': ['CBS (A>G)', 'G'],
                              'rs1801198': ['TCN2 (C>G)', 'G'],
                              'rs6875201': ['BHMT (A>G)', 'G'],
                              'rs1051266': ['SLC19A1 (T>C)', 'T']}

    dict_vitaminD_table = {'rs2228570': ['VDR Fok I (C>T)', 'T'],
                           'rs731236': ['VDR Taq I (A>G)', 'G'],
                           'rs2282679': ['GC', 'G']}

    dict_vitamins_table = {'rs7501331': ['BCMO1 (C>T)', 'T'],
                           'rs12934922': ['BCMO1 (A>T)', 'T'],
                           'rs964184': ['BUD13 (C>G)', 'C'],
                           'rs33972313': ['SLC23A1 (C>T)', 'T'],
                           'rs4654748': ['NBPF3 (C>T)', 'C'],
                           'rs602662': ['FUT2 (A>G)', 'G'],
                           'rs1051266': ['SLC19A1 (T>C)', 'T'],
                           'rs2236225': ['MTHFD1 (G>A)', 'G'],
                           'rs3199966': ['SLC44A1 (T>G)', 'G'],
                           'rs7946': ['PEMT (C>T)', 'T'],
                           'rs1557502': ['CHKB (C>T)', 'T'],
                           'rs10791957': ['CHKA (C>A)', 'A'],
                           'rs9001': ['CHDH (T>G)', 'G'],
                           'rs12676': ['CHDH (A>C)', 'A'],
                           'rs2266782': ['FMO3 (G>A)', 'A']}

    dict_mind_table = {'rs4680': ['COMT (G>A)', 'A'],
                       'rs6311': ['HTR2A (T>C)', 'C']}

    dict_lactase_table = {'rs4988235': ['MCM6 (G>A)', 'G']}

    celiac_rs_list = ['rs2187668', 'rs7454108',
                      'rs2395182', 'rs7775228', 'rs4639334']

    dict_celiac_table = {'rs7454108': ['HLA rs7454108', 'G'],
                         'rs2395182': ['HLA rs2395182', 'T'], 
                         'rs7775228': ['HLA rs7775228', 'G'], 
                         'rs4639334': ['HLA rs4639334', 'A']}

    dict_caffeine_table = {'rs762551': ['CYP1A2 (C>A)', 'C']}

    dict_alcohol_table = {'rs671': ['ALDH2 (A>G)', 'A']}

    dict_stress_table = {'rs4880': ['SOD2 (A>G)', 'G'],
                         'rs1050450': ['GPX1 (G>A)', 'A'],
                         'rs1001179': ['CAT (C>T)', 'T'],
                         'rs1799945': ['HFE (C>G)', 'G'],
                         'rs1800562': ['HFE (G>A)', 'A'],
                         'rs1695': ['GSTP1 (A>G)', 'G'],
                         'rs1138272': ['GSTP1 (C>T)', 'T'],
                         'rs1051740': ['EPHX1 (T>C)', 'C'],
                         'rs1800629': ['TNF-alfa (G>A)', 'A'],
                         'rs361525': ['TNF-alfa (G>A)', 'A'],
                         'rs1800795': ['IL6 (G>C)', 'G'],
                         'rs1800566': ['NQO1 (G>A)', 'A'],
                         'rs6721961': ['NRF2 (T>G)', 'T'],
                         'rs107251': ['SIRT6 (C>T)', 'T'],
                         'rs4986790': ['TLR4 (A>G)', 'G'],
                         'rs1050757': ['G6PD (T>C)', 'C'],
                         'rs2071429': ['G6PD (A>G)', 'G'],
                         'rs2230037': ['G6PD (C>T)', 'T']}

    dict_bioenergetic_table = {'rs8192678': ['PGC1-alfa (C>T)', 'T'],
                               'rs2016520': ['PPAR-delta (C>T)', 'T'],
                               'rs1799983': ['ENOS (G>T)', 'T'],
                               'rs1049434': ['SLC16A1 (T>A)', 'A'],
                               'rs1801260': ['CLOCK (A>G)', 'G'],
                               'rs17602729': ['AMPD1 (G>A)', 'A'],
                               'rs8111989': ['CKMM (T>C)', 'C'],
                               'rs1800592': ['UCP1 (T>C)', 'T'],
                               'rs4994': ['ADRB3 (A>G)', 'G'],
                               'rs679620': ['MMP3 (T>C)', 'C'],
                               'rs1805086': ['MSTN (T>C)', 'C']}

    dict_sportomics_table = {'rs699': ['AGT', ''],
                             'rs1799752': ['ECA', ''],
                             'rs5810761': ['BDKRB2', ''],
                             'rs1815739': ['ACTN3', '']}

    dict_cardiorisk_table = {'rs6025': ['Fator V de Leiden (C>T)', 'T'],
                             'rs1800595': ['Fator V haplótipo R2 (T>C)', 'C'],
                             'rs1799963': ['Protrombina G20210A', 'A'],
                             'rs5985': ['F.XIII V34L (C>A)', 'A'],
                             'rs1800790': ['Beta Fibrinogênio 455G>A', 'A'],
                             'rs5918': ['HPA1 L33P (T>C)', 'C'],
                             'rs1801133': ['MTHFR C677T', 'A'],
                             'rs1801131': ['MTHFR A1298C', 'G'],
                             'rs5742904': ['ApoB R3500Q (C>T)', 'T']}
    # 'rs4343': ['ECA del. Elemento ALU (G>A)', 'G'],

    dict_skindna_table = {'rs1131341': ['NQO1 (G>A)', 'A'],
                          'rs1800566': ['NQO1 (G>A)', 'A'],
                          'rs361525': ['TNF-alfa (G>A)', 'A'],
                          'rs1800629': ['TNF-alfa (G>A)', 'A'],
                          'rs1051740': ['EPHX1 (T>C)', 'C'],
                          'rs2234922': ['EPHX1 (A>G)', 'G'],
                          'rs1695': ['GSTP1 (A>G)', 'G'],
                          'rs4880': ['SOD2 (A>G)', 'G'],
                          'rs8192288': ['SOD3 (G>T)', 'T'],
                          'rs1001179': ['CAT (C>T)', 'T'],
                          'rs1050450': ['GPX1 (G>A)', 'A'],
                          'rs61816761': ['FLG (G>A)', 'A']}

    dict_alopecia_table = {'rs6152': ['AR', 'G']}

    dict_rs429358 = {'TT': 'E2', 'CC': 'E4', 'TC': 'E3', 'CT': 'E3'}

    dict_rs7412 = {'TT': 'E2', 'CC': 'E4', 'TC': 'E3', 'CT': 'E3'}

    # Define the path to the pictures
    red_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha vermelha.jpg'alt='Image not found'>"
    yellow_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha amarela.jpg'alt='Image not found'>"
    green_ball = "<img src='/home/gabriel/Documents/Repos/centro_de_genomas/Osteo/images/bolinha verde.jpg'alt='Image not found'>"

    # Dict for converting 'balls' into colors
    dict_balls_to_colors = {
        red_ball: 'red_ball',
        yellow_ball: 'yellow_ball',
        green_ball: 'green_ball'}

    # Set HTML head to use UTF-8
    html_head =  '''<!DOCTYPE html>
    <html>
      <head>
        <meta charset="utf-8">
        <title></title>
        <link rel="stylesheet" href="/home/gabriel/Documents/Repos/centro_de_genomas/css_models/tables_style.css">
      </head>
      '''

    patient_header = '''
                <p>Nome: %s</p>
                <p>Idade: %s</p>
                <p>Número de recipiente: %s</p>
                <p>Número de atendimento: %s</p>
                <p>Número de prontuário: %s</p>
                <p>Exame: %s</p>
                    '''

    html_title = '''
          <h2>%s</h2>
    '''
    # Create dataframe to control differences on snps related to guidelines
    df_inconsistency = pd.DataFrame(
        columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT'])

    # Create folder to receive the outputs
    output_folder_name = '4p_' + datetime.now().strftime("%Y-%m-%d")
    if output_folder_name not in os.listdir(os.getcwd()):
        os.mkdir(output_folder_name)

        # Let pandas receive long string
    pd.set_option('display.max_colwidth', -1)

    # Try to open csv
    file_error = False
    try:
        df = pd.read_csv(tsv_file, sep='\t')
    except IOError:
        print("Arquivo %s não encontrado." % csv_file)
        file_error = True
    # If csv_file was correctly loaded
    if file_error == False:
        df = pd.read_csv(tsv_file, sep='\t')

    # Get list of samples
    list_samples = tuple(set(df['Sample Name']))
    print(list_samples)

    # Filter the df to get only hotspot variants
    df = df[df['Allele Source'] == 'Hotspot']

    # Iterate over list of samples processing each specific data
    for sample in list_samples:
        df_sample = df[df['Sample Name'] == sample]

        print('\n\n\n\n %s \n\n\n\n' % sample)

        df_sample.to_csv(str(sample) + '_sample.tsv', sep='\t')

        # Create folder to receive files from specific sample
        if sample not in os.listdir(os.getcwd()):
            try:
                os.mkdir(str(sample))
            except:
                print('Folder %s already exists.' % str(sample))

        # Create dataframes
        dataframe_obesity = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_lipids = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_diabetes = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_hypertension = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_folatecycle = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_vitaminD = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_vitamins = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_mind = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_lactase = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_celiac = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_caffeine = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_alcohol = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_stress = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_bioenergetic = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_sportomics = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_cardiorisk = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_skindna = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        dataframe_alopecia = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])

        # Get patient gender from Pleres
        try:
            patient_info_df = get_patient_data_from_pleres(
                sample_number.zfill(12))
            patient_name = str(patient_info_df.iloc[0]['cliente'])
            patient_age = str(patient_info_df.iloc[0]['idade'])
            recipient_number = str(patient_info_df.iloc[0][
                                   'Unidade']) + '-' + str(patient_info_df.iloc[0]['Atendimento'])
            report_number = str(patient_info_df.iloc[0]['prontuario'])
            assay_name = str(patient_info_df.iloc[0]['Exame'])
            gender = str(patient_info_df.iloc[0]['sexo'])

        except:
            print("Impossibilidade de conexão com Pleres.")
            patient_name = ''
            patient_age = ''
            recipient_number = ''
            report_number = ''
            assay_name = ''
            gender = 'M'

        # Set patient header with data from Pleres
        patient_header_Pleres = patient_header % (patient_name,
                                                  patient_age,
                                                  str(sample).zfill(12),
                                                  recipient_number,
                                                  report_number,
                                                  assay_name,)

        # For obesity table
        for key, value in dict_obesity_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
                                         str(key), 'Sample Name'].iloc[0]

            print('Result: %s' % result)
            print('Ref: %s' % reference_allele)
            print('Alt: %s' % alternative_allele)
            print('Sample in df_sample: %s' % sample_in_df)

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_obesity.loc[len(dataframe_obesity)] = [  gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_obesity
        obesity_rs_order = ['rs9939609', 'rs8050136', 'rs1042713', 'rs1042714',
                            'rs7799039', 'rs1805094', 'rs17782313', 'rs12970134', 'rs2229616',
                            'rs1801282', 'rs925946', 'rs7498665', 'rs7566605', 'rs894160',
                            'rs7647305', 'rs5443', 'rs10938397', 'rs659366', 'rs17300539',
                            'rs28932472', 'rs5082']

        dataframe_obesity['dbSNP_index'] = dataframe_obesity['dbSNP']
        dataframe_obesity = dataframe_obesity.set_index('dbSNP_index')
        dataframe_obesity = dataframe_obesity.reindex(obesity_rs_order)

        # Write obesity table dataframe to HTML
        obesity_title = str(html_title % ('Tabela Obesidade',))

        with open(str(sample) + "_obesidade_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + obesity_title +
                               dataframe_obesity.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_obesidade_4p.html",
                         str(sample) + "_obesidade_4p.pdf")

        # For lipids table
        for key, value in dict_lipids_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            '''sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
                                         str(key), 'Sample Name'].iloc[0]'''

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_lipids.loc[len(dataframe_lipids)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Insert row for APOE on lipid dataframe
        dataframe_lipids.loc[len(dataframe_lipids)] = ['APOE (SNPs)', 'rs429358/rs7412*', '', '', '', '', '', 'MANUAL']
        dataframe_lipids.loc[len(dataframe_lipids)] = ['APOE GENOTIPO ', 'rs429358/rs7412', 'E4', '', '', '', '', 'MANUAL']

        # Parsing FABP2
        rs1799883_result = df_sample.loc[df_sample['Allele Name']
                                         == 'rs1799883', 'Allele Call'].iloc[0]
        FABP2_dbsnp = 'rs1799883'
        FABP2_gene = 'FABP2 (G>A)'
        reference_allele_FABP2 = df_sample.loc[
            df_sample['Allele Name'] == 'rs1799883', 'Ref'].iloc[0]
        alternative_allele_FABP2 = df_sample.loc[
            df_sample['Allele Name'] == 'rs1799883', 'Variant'].iloc[0]

        if gender == "M":
            risk_FABP2 = 'A'
            # Setting the risk balls figs
            try:
                analysis_FABP2, genotype_FABP2 = get_genotype_risk(rs1799883_result, risk_FABP2,
                                                                   reference_allele_FABP2, alternative_allele_FABP2)
            except:
                result_FABP2 = "error"
            # print('RESULT VALUE:' + str(rs1799883_result) + '\t' +
            # str(result_value))

        elif gender == "F":
            risk_FABP2 = 'G'
            # Setting the risk balls figs
            try:
                analysis_FABP2, genotype_FABP2 = get_genotype_risk(rs1799883_result, risk_FABP2,
                                                                   reference_allele_FABP2, alternative_allele_FABP2)
            except:
                result_FABP2 = "error"
            # print('RESULT VALUE:' + str(rs1799883_result) + '\t' +
            # str(result_value))

        else:
            result_FABP2 = "error"
            # print('RESULT VALUE:' + str(rs1799883_result) + '\t' +
            # str(result_value))

        # Write down FABP2 result to dataframe
        dataframe_lipids.loc[len(dataframe_lipids)] = [FABP2_gene, FABP2_dbsnp,
                                                       risk_FABP2, reference_allele_FABP2, alternative_allele_FABP2,
                                                       rs1799883_result, genotype_FABP2, analysis_FABP2]

        # Reorder the dataframe_lipids
        lipids_rs_order = ['rs1799837', 'rs662799', 'rs5128', 'rs693', 'rs429358/rs7412*',
                           'rs429358/rs7412', 'rs688', 'rs5925', 'rs708272', 'rs6756629', 'rs4299376', 'rs3846662',
                           'rs2070895', 'rs1800588', 'rs13702', 'rs1799883', 'rs964184', 'rs11206510',
                           'rs174546', 'rs174537', 'rs174616', 'rs953413']

        dataframe_lipids['dbSNP_index'] = dataframe_lipids['dbSNP']
        dataframe_lipids = dataframe_lipids.set_index('dbSNP_index')
        dataframe_lipids = dataframe_lipids.reindex(lipids_rs_order)

        # Write obesity table dataframe to HTML
        lipids_title = str(html_title % ('Tabela Lipídeos',))

        with open(str(sample) + "_lipideos_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + lipids_title +
                               dataframe_lipids.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_lipideos_4p.html",
                         str(sample) + "_lipideos_4p.pdf")

        # For diabetes table
        for key, value in dict_diabetes_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
                                         str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)
            # analysis = get_genotype_out[0]
            # result = get_genotype_out[1]

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_diabetes.loc[len(dataframe_diabetes)] = [    gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_diabetes
        diabetes_rs_order = ['rs1801282', 'rs7903146', 'rs5219', 'rs13266634',
                             'rs4402960', 'rs1800795', 'rs10938397', 'rs2943641', 'rs10830963', 'rs6234']

        dataframe_diabetes['dbSNP_index'] = dataframe_diabetes['dbSNP']
        dataframe_diabetes = dataframe_diabetes.set_index('dbSNP_index')
        dataframe_diabetes = dataframe_diabetes.reindex(diabetes_rs_order)

        # Write diabetes table dataframe to HTML
        diabetes_title = str(html_title % ('Tabela Diabetes',))
        with open(str(sample) + "_diabetes_4p.html", 'w') as diabetes_html:
            diabetes_html.write(html_head + patient_header_Pleres + diabetes_title +
                                dataframe_diabetes.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_diabetes_4p.html",
                         str(sample) + "_diabetes_4p.pdf")

        # For hypertension table
        for key, value in dict_systemic_arterical_hypertension.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
                                         str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_hypertension.loc[len(dataframe_hypertension)] = [            gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Write down rs4343 to dataframe (manually)
        dataframe_hypertension.loc[len(dataframe_hypertension)] = [        'ECA del. Elemento ALU (G>A)', 'rs4343', 'G', '', '', '', '', 'MANUAL']

        # Write dataframe_systemic_arterical_hypertension to HTML
        hypertension_title = str(html_title % (
            'Tabela de Hipertensão Arterial Sistêmica',))
        with open(str(sample) + "_hipertensao_arterial_4p.html", 'w') as hypertension_html:
            hypertension_html.write(html_head + patient_header_Pleres + hypertension_title +
                                    dataframe_hypertension.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_hipertensao_arterial_4p.html",
                         str(sample) + "_hipertensao_arterial_4p.pdf")

        # For folate cycle table
        for key, value in dict_folatecycle_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
                                         str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_folatecycle.loc[len(dataframe_folatecycle)] = [          gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_folatecycle
        folate_rs_order = ['rs1801133', 'rs1801131', 'rs1051266', 'rs1805087', 'rs1801394',
                           'rs1801198', 'rs234706', 'rs6875201']

        dataframe_folatecycle['dbSNP_index'] = dataframe_folatecycle['dbSNP']
        dataframe_folatecycle = dataframe_folatecycle.set_index('dbSNP_index')
        dataframe_folatecycle = dataframe_folatecycle.reindex(folate_rs_order)

        # Write dataframe_folatecycle to HTML
        folatecycle_title = str(html_title % ('Tabela Ciclo do Folato',))
        with open(str(sample) + "_folato_4p.html", 'w') as folate_html:
            folate_html.write(html_head + patient_header_Pleres + folatecycle_title +
                              dataframe_folatecycle.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_folato_4p.html",
                         str(sample) + "_folato_4p.pdf")

        # For Vitamin D table
        for key, value in dict_vitaminD_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]
            #sample_in_df = df_sample.loc[
            #    df_sample['Allele Name'] == str(key), 'Sample Name'].iloc[0]
            
            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"


            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_vitaminD.loc[len(dataframe_vitaminD)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_vitaminD
        vitaminD_rs_order = ['rs2228570', 'rs731236', 'rs2282679']

        dataframe_vitaminD['dbSNP_index'] = dataframe_vitaminD['dbSNP']
        dataframe_vitaminD = dataframe_vitaminD.set_index('dbSNP_index')
        dataframe_vitaminD = dataframe_vitaminD.reindex(vitaminD_rs_order)

        # Write dataframe_vitaminD to HTML
        vitaminD_title = str(html_title % ('Tabela Vitamina D',))
        with open(str(sample) + "_vitaminaD_4p.html", 'w') as vitaminD_html:
            vitaminD_html.write(html_head + patient_header_Pleres + vitaminD_title +
                                dataframe_vitaminD.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_vitaminaD_4p.html",
                         str(sample) + "_vitaminaD_4p.pdf")

        # For vitamins table
        for key, value in dict_vitamins_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
             #                            str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_vitamins.loc[len(dataframe_vitamins)] = [    gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_obesity
        vitamins_rs_order = ['rs7501331', 'rs12934922', 'rs964184', 'rs33972313', 'rs4654748',
                             'rs602662', 'rs1051266', 'rs2236225', 'rs3199966', 'rs7946', 'rs1557502', 'rs10791957',
                             'rs9001', 'rs12676', 'rs2266782']

        dataframe_vitamins['dbSNP_index'] = dataframe_vitamins['dbSNP']
        dataframe_vitamins = dataframe_vitamins.set_index('dbSNP_index')
        dataframe_vitamins = dataframe_vitamins.reindex(vitamins_rs_order)

        # Write obesity table dataframe to HTML
        vitamins_title = str(html_title % ('Tabela Vitaminas',))

        with open(str(sample) + "_vitaminas_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + vitamins_title +
                               dataframe_vitamins.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_vitaminas_4p.html",
                         str(sample) + "_vitaminas_4p.pdf")

        # For mind table
        for key, value in dict_mind_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
             #                            str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_mind.loc[len(dataframe_mind)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_mind
        mind_rs_order = ['rs4680', 'rs6311']

        dataframe_mind['dbSNP_index'] = dataframe_mind['dbSNP']
        dataframe_mind = dataframe_mind.set_index('dbSNP_index')
        dataframe_mind = dataframe_mind.reindex(mind_rs_order)

        # Write mind table dataframe to HTML
        mind_title = str(html_title % ('Tabela MIND',))

        with open(str(sample) + "_mind_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + mind_title +
                               dataframe_mind.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_mind_4p.html",
                         str(sample) + "_mind_4p.pdf")

        # For lactase table
        for key, value in dict_lactase_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
             #                            str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_lactase.loc[len(dataframe_lactase)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Write dataframe_lactase dataframe to HTML
        lactase_title = str(html_title % ('Tabela Lactose',))

        with open(str(sample) + "_lactose_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + lactase_title +
                               dataframe_lactase.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_lactose_4p.html",
                         str(sample) + "_lactose_4p.pdf")

        # For celiac table
        hla_haplotype = 'Ausência de alelos de risco'
        
        for key, value in dict_celiac_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
             #                            str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            #GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE']

            dataframe_celiac.loc[len(dataframe_celiac)] = [gene, dbsnp, risk, reference_allele,
                                                           alternative_allele, result, genotype, analysis]

            #dataframe_celiac.loc[len(dataframe_mind)] = [gene, dbsnp, risk, reference_allele, 
            #                                              alternative_allele, result, genotype, analysis]
        
        '''for rs in celiac_rs_list:
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = rs
            gene = 'HLA'

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
                                         str(key), 'Sample Name'].iloc[0]

            # Setting the risk balls figs
            try:
                result_value = result.replace('/', '')
            except:
                result_value = "error"

            if rs == 'rs2187668':
                if 'T' in result_value and analysis == green_ball:
                    analysis = 'ALTO'
                    hla_haplotype = 'HLA-DQ 2.5'

            elif rs == 'rs7454108':
                if 'G' in result_value and analysis == green_ball:
                    analysis = 'MEDIO'
                    hla_haplotype = 'HLA-DQ 8'

            elif rs == 'rs2395182':
                if 'T' in result_value and analysis == green_ball:
                    analysis = 'BAIXO'
                    hla_haplotype_rs2395182 = 'HLA-DQ 2.2'

            elif rs == 'rs7775228':
                if 'G' in result_value and haplotype_rs2395182 == 'HLA-DQ 2.2':
                    analysis = 'MEDIO'
                    hla_haplotype = 'HLA-DQ 2.2'

            elif rs == 'rs4639334' and analysis == green_ball:
                if 'A' in result_value:
                    analysis = 'MEDIO'
                    hla_haplotype = 'HLA-DQ 7'
            else:
                    analysis = 'BAIXO'

            try:
                result = result.replace('/', '')
            except:
                print('Problema removendo / de resultado')

            dataframe_celiac.loc[len(dataframe_celiac)] = [
                gene, dbsnp, '', '', '', result, '', '']
        '''
        dataframe_celiac.loc[len(dataframe_celiac)] = ['HLA rs2187668', 'rs2187668', '', '', '', '', '', '']
        


        # Setting the results and HLA values to dataframe
        dataframe_celiac.set_value((dataframe_celiac[dataframe_celiac[
                                   'dbSNP'] == 'rs2395182'].index), 'ANALISE', analysis)
        dataframe_celiac.set_value((dataframe_celiac[dataframe_celiac[
                                   'dbSNP'] == 'rs4639334'].index), 'ANALISE', hla_haplotype)

        # Reorder the dataframe_celiac
        celiac_rs_order = ['rs2187668', 'rs2395182', 'rs7775228', 
                           'rs4639334','rs7454108']

        dataframe_celiac['dbSNP_index'] = dataframe_celiac['dbSNP']
        dataframe_celiac = dataframe_celiac.set_index('dbSNP_index')
        dataframe_celiac = dataframe_celiac.reindex(celiac_rs_order)

        # Write celiac table dataframe to HTML
        celiac_title = str(html_title % ('Tabela Glúten',))

        with open(str(sample) + "_gluten_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + celiac_title +
                               dataframe_celiac.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_gluten_4p.html",
                         str(sample) + "_gluten_4p.pdf")

        # For caffeine table
        for key, value in dict_caffeine_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_caffeine.loc[len(dataframe_caffeine)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Write caffeine table dataframe to HTML
        caffeine_title = str(html_title % ('Tabela Cafeína',))

        with open(str(sample) + "_cafeina_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + caffeine_title +
                               dataframe_caffeine.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_cafeina_4p.html",
                         str(sample) + "_cafeina_4p.pdf")

        # For alcohol table
        for key, value in dict_alcohol_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_alcohol.loc[len(dataframe_alcohol)] = [  gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Write alcohol table dataframe to HTML
        alcohol_title = str(html_title % ('Tabela Álcool',))

        with open(str(sample) + "_alcool_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + alcohol_title +
                               dataframe_alcohol.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_alcool_4p.html",
                         str(sample) + "_alcool_4p.pdf")

        # For stress table
        for key, value in dict_stress_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_stress.loc[len(dataframe_stress)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Processing SNPs with indels
        stress_indel_vars = {'rs2071487': ['GSTM1 del', 'DEL'],
                             'rs74837985': ['GSTM1 del', 'DEL'],
                             'rs2266633': ['GSTT1 del', 'DEL'],
                             'rs2266637': ['GSTT1 del', 'DEL']}

        for key, value in stress_indel_vars.items():
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            # Get the genotype
            if key in ['rs2266633', 'rs2266637']:
                if result == 'Homozygous' or result == 'Absent':
                    genotype = 'INS'
                    analysis = 'BAIXO'

                elif result == 'No call':
                    genotype = 'DEL'
                    analysis = 'ALTO'

            elif key in ['rs2071487', 'rs74837985']:
                if result == 'Homozygous' or result == 'Absent':
                    genotype = 'INS'
                    analysis = 'BAIXO'

                elif result == 'No call':
                    genotype = 'DEL'
                    analysis = 'ALTO'

            dataframe_stress.loc[len(dataframe_stress)] = [
                gene, dbsnp, risk, '', '', result, genotype, analysis]

        # Reorder the dataframe_stress
        stress_rs_order = ['rs4880', 'rs1050450', 'rs1001179', 'rs1799945', 'rs1800562', 'rs2071487',
                           'rs74837985', 'rs1695', 'rs1138272', 'rs2266633', 'rs2266637', 'rs1051740', 'rs1800629',
                           'rs361525', 'rs1800795', 'rs1800566', 'rs6721961', 'rs107251', 'rs4986790', 'rs1050757',
                           'rs2071429', 'rs2230037']

        dataframe_stress['dbSNP_index'] = dataframe_stress['dbSNP']
        dataframe_stress = dataframe_stress.set_index('dbSNP_index')
        dataframe_stress = dataframe_stress.reindex(stress_rs_order)

        # Write stress table dataframe to HTML
        stress_title = str(html_title % ('Tabela Estresse',))

        with open(str(sample) + "_estresse_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + stress_title +
                               dataframe_stress.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_estresse_4p.html",
                         str(sample) + "_estresse_4p.pdf")

        # For bioenergetics table
        for key, value in dict_bioenergetic_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_bioenergetic.loc[len(dataframe_bioenergetic)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Reorder the dataframe_bioenergetic
        bioenergetic_rs_order = ['rs8192678', 'rs2016520', 'rs1799983', 'rs1049434',
                                 'rs1801260', 'rs17602729', 'rs8111989', 'rs1800592', 'rs4994', 'rs679620', 'rs1805086']

        dataframe_bioenergetic['dbSNP_index'] = dataframe_bioenergetic['dbSNP']
        dataframe_bioenergetic = dataframe_bioenergetic.set_index(
            'dbSNP_index')
        dataframe_bioenergetic = dataframe_bioenergetic.reindex(
            bioenergetic_rs_order)

        # Write bioenergetics table dataframe to HTML
        bioenergetics_title = str(html_title % ('Tabela Bioenergética',))

        with open(str(sample) + "_bioenergetica_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + bioenergetics_title +
                               dataframe_bioenergetic.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_bioenergetica_4p.html",
                         str(sample) + "_bioenergetica_4p.pdf")

        # For sportomics table
        for key, value in dict_sportomics_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_sportomics.loc[len(dataframe_sportomics)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Write down rs4343 to dataframe (manually)
        dataframe_sportomics.loc[len(dataframe_hypertension)] = [
            'ECA del. Elemento ALU (G>A)', 'rs4343', 'G', '', '', '', '', 'MANUAL']

        # Reorder the dataframe_sportomics
        sportomics_rs_order = ['rs699', 'rs1799752', 'rs5810761', 'rs1815739']

        dataframe_sportomics['dbSNP_index'] = dataframe_sportomics['dbSNP']
        dataframe_sportomics = dataframe_sportomics.set_index('dbSNP_index')
        dataframe_sportomics = dataframe_sportomics.reindex(
            sportomics_rs_order)

        # Write sportomics table dataframe to HTML
        sportomics_title = str(html_title % ('Tabela Sportomics',))

        with open(str(sample) + "_sportomics_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + sportomics_title +
                               dataframe_sportomics.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_sportomics_4p.html",
                         str(sample) + "_sportomics_4p.pdf")

        # For cardiorisk table
        for key, value in dict_cardiorisk_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_cardiorisk.loc[len(dataframe_cardiorisk)] = [gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]
        
        # Write down rs4343 to dataframe (manually)
        dataframe_cardiorisk.loc[len(dataframe_cardiorisk)] = ['ECA del. Elemento ALU (G>A)', 'rs4343', 'G', '', '', '', '', 'MANUAL']

        # For rs587776796 (PAI-1 (4G/5G))
        dict_rs587776796 = {'rs587776796': ['PAI-1 (4G/5G)', '4G']}

        dbsnp_rs587776796 = list(dict_rs587776796.keys())[0]
        gene_rs587776796 = dict_rs587776796[dbsnp_rs587776796][0]
        risk_rs587776796 = dict_rs587776796[dbsnp_rs587776796][1]
        genotype_rs587776796 = ''

        try:
            result_rs587776796 = df_sample.loc[
                df_sample['Allele Name'] == 'rs587776796', 'Allele Call'].iloc[0]
        except:
            result_rs587776796 = "error"

        if result_rs587776796 == 'Absent':
            genotype_rs587776796 = '4G/4G'
            analysis_rs587776796 = 'ALTO'

        elif result_rs587776796 == 'Heterozygous':
            genotype_rs587776796 = '4G/5G'
            analysis_rs587776796 = 'INTERMEDIARIO'

        elif result_rs587776796 == 'Homozygous':
            genotype_rs587776796 = '5G/5G'
            analysis_rs587776796 = 'BAIXO'

        dataframe_cardiorisk.loc[len(dataframe_cardiorisk)] = [gene_rs587776796, dbsnp_rs587776796, risk_rs587776796, '', '', result_rs587776796,
                                                               genotype_rs587776796, analysis_rs587776796]

        # Reorder the dataframe_cardiorisk
        cardiorisk_rs_order = ['rs6025', 'rs1800595', 'rs1799963', 'rs5985', 'rs1800790', 'rs587776796',
                               'rs5918', 'rs1801133', 'rs1801131', 'rs5742904']

        dataframe_cardiorisk['dbSNP_index'] = dataframe_cardiorisk['dbSNP']
        dataframe_cardiorisk = dataframe_cardiorisk.set_index('dbSNP_index')
        dataframe_cardiorisk = dataframe_cardiorisk.reindex(
            cardiorisk_rs_order)

        dataframe_cardiorisk.loc[len(dataframe_cardiorisk)] = ['ApoE haplótipo', '', '', '', '', '', '', 'MANUAL']

        # Write cardiorisk table dataframe to HTML
        cardiorisk_title = str(html_title % ('Tabela Cardiorisk',))

        with open(str(sample) + "_cardiorisk_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + cardiorisk_title +
                               dataframe_cardiorisk.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_cardiorisk_4p.html",
                         str(sample) + "_cardiorisk_4p.pdf")

        # For skindna table
        for key, value in dict_skindna_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_skindna.loc[len(dataframe_skindna)] = [  gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # For variants with indels
        dict_skindna_indels = {'rs1799750': ['MMP1 (D>I)', 'INS'],
                               'rs558269137': ['FLG (I>D)', 'DEL']}

        for key, value in dict_skindna_indels.items():

            dbsnp_indel = key
            gene_indel = value[0]
            risk_indel = value[1]
            genotype_indel = ''
            analysis_indel = ''

            try:
                result_indel = df_sample.loc[df_sample['Allele Name']
                                             == key, 'Allele Call'].iloc[0]
            except:
                result_indel = "error"

            if key == 'rs1799750':
                if result_indel == 'Absent':
                    genotype_indel = 'INS/INS'
                    analysis_indel = 'ALTO'

                elif result_indel == 'Heterozygous':
                    genotype_indel = 'INS/DEL'
                    analysis_indel = 'INTERMEDIARIO'

                elif result_indel == 'Homozygous':
                    genotype_indel = 'DEL/DEL'
                    analysis_indel = 'BAIXO'

            elif key == 'rs558269137':
                if result_indel == 'Absent':
                    genotype_indel = 'INS/INS'
                    analysis_indel = 'BAIXO'

                elif result_indel == 'Heterozygous':
                    genotype_indel = 'INS/DEL'
                    analysis_indel = 'INTERMEDIARIO'

                elif result_indel == 'Homozygous':
                    genotype_indel = 'DEL/DEL'
                    analysis_indel = 'ALTO'

            dataframe_skindna.loc[len(dataframe_skindna)] = [  gene_indel, dbsnp_indel, risk_indel, '', '', result_indel, genotype_indel, analysis_indel]

        # Reorder the dataframe_skindna
        skindna_rs_order = ['rs1799750', 'rs1131341', 'rs1800566', 'rs361525', 'rs1800629', 'rs1051740',
                            'rs2234922', 'rs1695', 'rs4880', 'rs8192288', 'rs1001179', 'rs1050450', 'rs61816761', 'rs558269137']

        dataframe_skindna['dbSNP_index'] = dataframe_skindna['dbSNP']
        dataframe_skindna = dataframe_skindna.set_index('dbSNP_index')
        dataframe_skindna = dataframe_skindna.reindex(skindna_rs_order)

        # Write skindna table dataframe to HTML
        skindna_title = str(html_title % ('Tabela Skin DNA',))

        with open(str(sample) + "_skindna_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + skindna_title +
                               dataframe_skindna.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_skindna_4p.html",
                         str(sample) + "_skindna_4p.pdf")

        # For alopecia table
        for key, value in dict_alopecia_table.items():
            # Set the values for each row based on data from fixed tables
            # and results from raw data
            dbsnp = key
            gene = value[0]
            risk = value[1]

            try:
                reference_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Ref'].iloc[
                    0]
            except:
                reference_allele = 'ERROR'

            try:
                alternative_allele = df_sample.loc[df_sample['Allele Name'] == str(key), 'Variant'].iloc[
                    0]
            except:
                alternative_allele = 'ERROR'
            try:
                result = df_sample.loc[df_sample['Allele Name'] ==
                                       str(key), 'Allele Call'].iloc[0]
            except:
                result = "error"

            #sample_in_df = df_sample.loc[df_sample['Allele Name'] ==
            #                             str(key), 'Sample Name'].iloc[0]

            analysis, genotype = get_genotype_risk(
                result, risk, reference_allele, alternative_allele)

            if risk != reference_allele and risk != alternative_allele:
                df_inconsistency.loc[len(df_inconsistency)] = [
                    gene, dbsnp, risk, reference_allele, alternative_allele]

            dataframe_alopecia.loc[len(dataframe_alopecia)] = [    gene, dbsnp, risk, reference_allele, alternative_allele, result, genotype, analysis]

        # Write alopecia table dataframe to HTML
        alopecia_title = str(html_title % ('Tabela Alopécia',))

        with open(str(sample) + "_alopecia_4p.html", 'w') as current_html:
            current_html.write(html_head + patient_header_Pleres + alopecia_title +
                               dataframe_alopecia.to_html(escape=False, index=False))

        # Convert HTML to pdf
        pdfkit.from_file(str(sample) + "_alopecia_4p.html",
                         str(sample) + "_alopecia_4p.pdf")

        list_dfs = [dataframe_obesity, dataframe_lipids, dataframe_diabetes,
                    dataframe_hypertension, dataframe_folatecycle, dataframe_vitaminD,
                    dataframe_vitamins, dataframe_mind, dataframe_lactase, dataframe_celiac,
                    dataframe_caffeine, dataframe_alcohol, dataframe_stress, dataframe_bioenergetic,
                    dataframe_sportomics, dataframe_cardiorisk, dataframe_skindna, dataframe_alopecia]

        list_titles = ['Perfil de Predisposicao a Obesidade', 'Perfil Lipidico', 'Perfil Diabetes',
                       'Hipertensao Arterial Sistemica', 'Perfil do Ciclo do Folato', 'Perfil para Vitamina D',
                       'Perfil Vitaminas', 'Perfil MIND', 'Perfil Lactase', 'Perfil Celiaco', 'Perfil Cafeina',
                       'Metabolismo de Alcool', 'Perfil de Estresse', 'Perfil Bioenergetico', 'Sportomics',
                       'Cardiorisk', 'Skin DNA (Derma)', 'Alopecia Androgenica Masculina']

        # Create dataframe to receive titles
        dataframe_titles = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])

        for name in list_titles:
            dataframe_titles.loc[len(dataframe_titles)] = [
                name, '', '', '', '', '', '', '']

        # Create empty dataframe
        empty_df = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])
        empty_df.loc[len(empty_df)] = ['', '', '', '', '', '', '', '']


        concatenated_dfs = pd.DataFrame(
            columns=['GENE', 'dbSNP', 'RISCO', 'REF', 'ALT', 'RESULTADO', 'GENOTIPO', 'ANALISE'])

        for n in range(18):
            concatenated_dfs = pd.concat(
                [concatenated_dfs, dataframe_titles.iloc[[n]], list_dfs[n], empty_df])

        # concatenated_dfs = pd.concat(list_dfs)

        concatenated_dfs.to_csv(str(sample) + '_concat.tsv', sep='\t', index=False)

        # Moving files to folder sample folder
        for item in os.listdir(os.getcwd()):
            if os.path.isfile(item) and str(sample) in item:
                if 'laudo' not in item:
                    try:
                        shutil.move(item, os.getcwd() + "/" + str(sample))
                    except:
                        print(str(_file) + " já na pasta.")
                elif 'laudo' in item:
                    try:
                        shutil.move(item, output_folder_name)
                    except:
                        print(str(_file) + " já na pasta.")
        try:
            shutil.move(str(sample), output_folder_name)
        except:
            print('Erro ao mover ' + str(sample) +
                  ' para ' + output_folder_name)

        df_inconsistency = df_inconsistency.drop_duplicates(
            subset=None, keep='first', inplace=False)
        df_inconsistency.to_csv('dataframe_inconsistencias.tsv', sep='\t')

    return(df_inconsistency)
